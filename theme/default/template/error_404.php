<?
	
	$MainContent.="
	    <div style=\"text-align: center;\">
		    <table style=\"padding: 20px; background-color: silver; color: black; font-size: 19px; border-style: solid; border-width: 5px; border-color: blue;\">
		        <tr>
					<td><img src=\"./theme/{$_REQUEST["Theme"]}/image/other/error_404.gif\" style=\"height: 185px; border-style: solid; border-width: 5px; border-color: gray;\"></td>
					<td style=\"padding: 10px; text-align: center;\">
						This area is under construction.<br>
						<br>
						Please check back later.<br>
						<br>
						Thank you.<br>
						<br>
						{$Application["Title"]}
					</td>
				</tr>
			</table>
		</div>
	";
	
	//Email the webmaster that the requested page was not found!
	SendMail(
		$ToEmail=$Application["EmailSupport"],
		$Subject="Page missing!",
		$Body="
		    The '<b>./script/{$_REQUEST["Script"]}.php</b>' was missing while '<b>{$_SESSION["UserName"]}</b> ({$_SESSION["UserTypeName"]})' requested it.<br>
		    <br>
		    <a href=\"".ApplicationURL($Script="")."\">{$Application["Title"]}</a>
		",
		$FromName=$Application["Title"],
		$FromEmail = $Application["EmailContact"],
		$ReplyToName=$Application["Title"],
		$ReplyToEmail=$Application["EmailContact"],
		$ExtraHeaderParameters=""
	);
?>
