<?
	$TinyMCE_HTML="";
	if(isset($_REQUEST["UseTinyMCE"]))$TinyMCE_HTML.="
		<!-- TinyMCE -->
		<script language=\"javascript\" type=\"text/javascript\" src=\"./library/tinymce/tiny_mce.js\"></script>
		<script language=\"javascript\" type=\"text/javascript\">
			tinyMCE.init({
				theme : \"advanced\",
				mode : \"exact\",
				elements : \"StaticContent, UserTypeDescription, PageContent\",
				plugins : \"style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,codeprotect,advimage\",
				theme_advanced_buttons1_add_before : \"save,newdocument,separator\",
				theme_advanced_buttons1_add : \"fontselect,fontsizeselect\",
				theme_advanced_buttons2_add : \"separator,insertdate,inserttime,preview,separator,forecolor,backcolor\",
				theme_advanced_buttons2_add_before: \"cut,copy,paste,pastetext,pasteword,separator,search,replace,separator\",
				theme_advanced_buttons3_add_before : \"tablecontrols,separator\",
				theme_advanced_buttons3_add : \"emotions,iespell,flash,advhr,separator,print,separator,ltr,rtl,separator,fullscreen\",
				theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops\",
				theme_advanced_toolbar_location : \"top\",
				theme_advanced_toolbar_align : \"left\",
				theme_advanced_path_location : \"bottom\",
				content_css : \"example_full.css\",
			    plugin_insertdate_dateFormat : \"%Y-%m-%d\",
			    plugin_insertdate_timeFormat : \"%H:%M:%S\",
				extended_valid_elements : \"hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]\",
				external_link_list_url : \"example_link_list.js\",
				external_image_list_url : \"example_image_list.js\",
				flash_external_list_url : \"example_flash_list.js\",
				file_browser_callback : \"fileBrowserCallBack\",
				theme_advanced_resize_horizontal : false,
				theme_advanced_resizing : true
			});

			function fileBrowserCallBack(field_name, url, type, win) {
				// This is where you insert your custom filebrowser logic
				alert(\"Example of filebrowser callback: field_name: \" + field_name + \", url: \" + url + \", type: \" + type);

				// Insert new URL, this would normaly be done in a popup
				win.document.forms[0].elements[field_name].value = \"someurl.htm\";
			}
		</script>
		<!-- /TinyMCE -->
	";
	
	$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
	if (preg_match('/opera/', $userAgent)) { 
		 $browername = 'opera'; 
	 } 
	 elseif (preg_match('/webkit/', $userAgent)) { 
		 $browername = 'safari'; 
	 } 
	 elseif (preg_match('/msie/', $userAgent)) { 
		 $browername = 'msie'; 
	 } 
	 elseif (preg_match('/mozilla/', $userAgent) && !preg_match('/compatible/', $userAgent)) { 
		 $browername = 'mozilla'; 
	 } 
	 elseif (preg_match('/chrome/', $userAgent) && !preg_match('/compatible/', $userAgent)) { 
		 $browername = 'chrome'; 
	 } 
	 else { 
		 $browername = 'unrecognized'; 
	 }
	
	//The output must be set for the $BeforeMainContent variable
	$BeforeMainContent.="
		<title>{$Application["Title"]}</title>
		<link rel=\"stylesheet\" type=\"text/css\" href=\"./theme/{$_REQUEST["Theme"]}/style/reset.css\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"./theme/{$_REQUEST["Theme"]}/style/layout.css\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"./theme/{$_REQUEST["Theme"]}/style/style.css\">
	    <link rel=\"stylesheet\" type=\"text/css\" href=\"./theme/{$_REQUEST["Theme"]}/style/datagrid.css\">

		<script type=\"text/javascript\" src=\"./theme/default/javascript/tinybox.js\"></script>
		<script language=\"javascript\" type=\"text/javascript\" src=\"./library/http.js\"></script>
		<script language=\"javascript\" type=\"text/javascript\" src=\"./library/dhtml.js\"></script>
		<script language=\"javascript\" type=\"text/javascript\" src=\"./library/string.js\"></script>
		<script language=\"javascript\" type=\"text/javascript\" src=\"./theme/default/javascript/jquery-1.5.2.min.js\"></script>
		<script type=\"text/javascript\" src=\"./theme/{$_REQUEST["Theme"]}/javascript/jquery00.js\"></script>
		<script type=\"text/javascript\" src=\"./theme/{$_REQUEST["Theme"]}/javascript/jcarouse.js\"></script>
		<script type=\"text/javascript\" src=\"./theme/{$_REQUEST["Theme"]}/javascript/forms000.js\"></script>
		<script type=\"text/javascript\" src=\"./theme/{$_REQUEST["Theme"]}/javascript/script00.js\"></script>
		<script type=\"text/javascript\" src=\"./theme/{$_REQUEST["Theme"]}/javascript/ie6_scri.js\"></script>	
		<script language=\"javascript\" type=\"text/javascript\" src=\"./theme/default/javascript/jquery.validate.min.js\"></script>
		<script type=\"text/javascript\" language=\"javascript\">
			$(document).ready(function(){
				$('input[type=\"text\"]').addClass('typeText');
				$('input[type=\"password\"]').addClass('typeText');
				$('textarea').addClass('typeText');
				if ( $.browser.msie ) {
					$('.container').css('overflow','inherit');
				} else {
					$('.container').css('overflow','hidden');
				}
				
			});
		</script>
		<!--[if lt IE 7]>
			 <link rel=\"stylesheet\" href=\"./theme/{$_REQUEST["Theme"]}/style/ie6.css\" type=\"text/css\" media=\"screen\">
			 <script type=\"text/javascript\" src=\"./theme/{$_REQUEST["Theme"]}/javascript/ie_png.js\"></script>
			 <script type=\"text/javascript\">
				ie_png.fix('.png, .link1, .link1 span, #booking-form a.search, #booking-form a.search span, #booking-form a.search b, #booking-form a.opt, .link2, .link2 span');
			 </script>
		<![endif]-->
		<!--[if IE]>
			<style>
				header {
					text-align:center;	
				}
				h1 a {
					float:none;
				}
				#content .row-1 .container .inside {
					padding-top:70px;	
				}
				.container {					
					text-align:left;
				}
				section {
					text-align:center;	
				}
				footer {
					text-align:center;	
				}
			</style>
			<script type=\"text/javascript\" src=\"./theme/{$_REQUEST["Theme"]}/javascript/html5.js\"></script>
		  <![endif]-->

		$TinyMCE_HTML
	";
?>