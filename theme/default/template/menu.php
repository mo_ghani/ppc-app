<?

	if ($browername == 'msie'){

	$MainContent.="

	<link rel=\"stylesheet\" type=\"text/css\" href=\"./theme/{$_REQUEST["Theme"]}/style/ddsmoothmenu.css\" />

	<script type=\"text/javascript\" src=\"./theme/{$_REQUEST["Theme"]}/javascript/ddsmoothmenu.js\"></script>

	<script type=\"text/javascript\">

		ddsmoothmenu.init({

				mainmenuid: \"smoothmenu2\", //Menu DIV id

				orientation: 'h', //Horizontal or vertical menu: Set to \"h\" or \"v\"

				classname: 'ddsmoothmenu', //class added to menu's outer DIV

				//customtheme: [\"#804000\", \"#482400\"],

				contentsource: \"markup\" //\"markup\" or [\"container_id\", \"path_to_menu_file\"]

			})

	</script>

	";

	}

	if($_SESSION["UserTypeID"]==$Application["UserTypeIDGuest"]){

	$MainContent.="

		  <nav>

			 <ul>

				<li><a href=\"".ApplicationURL($Script="home")."\"><span>Home</span></a></li>

				<li><a href=\"".ApplicationURL($Script="login")."\"><span>Advertisement</span></a></li>

				<li><a href=\"".ApplicationURL($Script="profile")."\"><span>Profile</span></a></li>
				<li><a href=\"".ApplicationURL($Script="mission")."\"><span>Our Mission</span></a></li>
				<li><a href=\"".ApplicationURL($Script="about")."\"><span>About us</span></a></li>

				<li><a href=\"".ApplicationURL($Script="contactus")."\"><span>Contacts</span></a></li>

				<li ><a href=\"".ApplicationURL($Script="usersignup")."\"><span>Register</span></a></li>

                <li ><a href=\"".ApplicationURL($Script="login")."\"><span>Login</span></a></li>

			 </ul>

		  </nav>

	";

	}



	if($_SESSION["UserTypeID"]==$Application["UserTypeIDMember"]){

		if ($browername == 'msie'){

	$MainContent.="

			<div id=\"smoothmenu2\" class=\"ddsmoothmenu\">

	";

		}else{

	$MainContent.="

			<nav>

	";

		}

	$MainContent.="

                <ul>

                    <li ><a href=\"".ApplicationURL($Script="memberhome")."\"><span>Home</span></a></li>

                    <li >

                        <a href=\"javascript:void(0)\"><span>My Account</span></a>

                        <ul>

                            <li class=\"first\"><a href=\"".ApplicationURL($Script="userprofile")."\"><span>Profile</span></a></li>

                            <li><a href=\"".ApplicationURL($Script="pincode")."\"><span>Pin Code</span></a></li>";

                            if ($_SESSION["rankid"]==6){
							
								$MainContent.="<li><a href=\"".ApplicationURL($Script="investment")."\"><span>Investment</span></a></li>";
							}
							
							$MainContent.="
							<li><a href=\"".ApplicationURL($Script="transfer")."\"><span>Transfer</span></a></li>

                            <li><a href=\"".ApplicationURL($Script="withdraw")."\"><span>Wire Transfer</span></a></li>

                            <li><a href=\"".ApplicationURL($Script="binarytree")."\"><span>View Tree</span></a></li>

							<li><a href=\"".ApplicationURL($Script="downline")."\"><span>Downline</span></a></li>

                            <li ><a href=\"".ApplicationURL($Script="changepass")."\"><span>Change Password</span></a></li>

							<li class=\"last\"><a href=\"".ApplicationURL($Script="changetpass")."\"><span>Change T.Password</span></a></li>

                        </ul>

                    </li>

                    <li >

						<a href=\"javascript:;\"><span>Report</span></a>

						<ul>

							<li><a href=\"".ApplicationURL($Script="sponsorreport")."\"><span>Direct Sponsor</span></a></li>

							<li><a href=\"".ApplicationURL($Script="matchingreport")."\"><span>Matching Report</span></a></li>";

                            if ($_SESSION["rankid"]==6){
							
								$MainContent.="<li><a href=\"".ApplicationURL($Script="investreport")."\"><span>Investment Report</span></a></li>";
							}
							
							$MainContent.="

                           	<li><a href=\"".ApplicationURL($Script="withdrawreport")."\"><span>Wire Transfer Report</span></a></li>

                            <li><a href=\"".ApplicationURL($Script="transferreport")."\"><span>Transfer Report</span></a></li>

                            <li class=\"last\"><a href=\"".ApplicationURL($Script="transreport")."\"><span>Transaction Report</span></a></li>

                        </ul>

					</li>";

					if ($_SESSION["rankid"]!=6){
					
						$MainContent.="<li ><a href=\"".ApplicationURL($Script="advertisementlist")."\"><span>Advertisement</span></a></li>";
					}
					
					$MainContent.="					

                    <li ><a href=\"".ApplicationURL($Script="logout")."\"><span>Logout</span></a></li>

                </ul>

	";

		if ($browername == 'msie'){

	$MainContent.="

			</div>

	";

		}else{

	$MainContent.="

			</nav>

	";

		}

	}



	if($_SESSION["UserTypeID"]==$Application["UserTypeIDAdministrator"]){

		if ($browername == 'msie'){

	$MainContent.="

			<div id=\"smoothmenu2\" class=\"ddsmoothmenu\">

	";

		}else{

	$MainContent.="

			<nav>

	";

		}

	$MainContent.="

                <ul>

                    <li ><a href=\"".ApplicationURL($Script="adminhome")."\"><span>Home</span></a></li>

                    <li >

                        <a href=\"javascript:void(0)\"><span>Manage</span></a>

                        <ul>

                            <li><a href=\"".ApplicationURL($Script="usermanage")."\"><span>Manage Member</span></a></li>

                            <li><a href=\"".ApplicationURL($Script="bankmanage")."\"><span>Manage Bank</span></a></li>

							<li><a href=\"".ApplicationURL($Script="toursmanage")."\"><span>Manage Tour</span></a></li>

                            <li><a href=\"".ApplicationURL($Script="pincodemanage")."\"><span>Manage Pincode</span></a></li>

							<li><a href=\"".ApplicationURL($Script="investsetting")."\"><span>Manage Investment</span></a></li>

							<li><a href=\"".ApplicationURL($Script="advertmanage")."\"><span>Manage Advertisement</span></a></li>

							<li><a href=\"".ApplicationURL($Script="sitesetting")."\"><span>Manage Site</span></a></li>

							<li class=\"last\"><a href=\"".ApplicationURL($Script="changepass")."\"><span>Change Password</span></a></li>

                        </ul>

                    </li>

                    <li >
						<a href=\"javascript:;\"><span>Payment</span></a>
						<ul>
							<li><a href=\"".ApplicationURL($Script="investmentactive")."\"><span>Investment</span></a></li>
							<li><a href=\"".ApplicationURL($Script="balancezero")."\"><span>Balance Zero</span></a></li>
                            <li><a href=\"".ApplicationURL($Script="paymentmanage")."\"><span>Balance Transfer</span></a></li>
                            <li><a href=\"".ApplicationURL($Script="transferAdminReport")."\"><span>Transfer Report</span></a></li>
							<li><a href=\"".ApplicationURL($Script="tourbuy")."\"><span>Tour Record</span></a></li>
							<li><a href=\"".ApplicationURL($Script="withdrawpayment")."\"><span>Wire Transfer Payment</span></a></li>
                        </ul>
					</li>

                    <li >

						<a href=\"javascript:void(0)\"><span>Page Content</span></a>

						<ul>

							<li><a href=\"".ApplicationURL($Script="about")."\"><span>About Us</span></a></li>

                            <li><a href=\"".ApplicationURL($Script="advertisementlist")."\"><span>Advertisement</span></a></li>

							<li><a href=\"".ApplicationURL($Script="policy")."\"><span>Privacy Policy</span></a></li>

							<li><a href=\"".ApplicationURL($Script="mission")."\"><span>Our Mission</span></a></li>

							<li><a href=\"".ApplicationURL($Script="profile")."\"><span>Company Profile</span></a></li>

							<!--<li><a href=\"".ApplicationURL($Script="message","UseTinyMCE")."\"><span>Message</span></a></li>

							<li><a href=\"".ApplicationURL($Script="profile")."\"><span>Message</span></a></li>

							<li><a href=\"".ApplicationURL($Script="products")."\"><span>Products</span></a></li>

                            <li><a href=\"".ApplicationURL($Script="ourteam")."\"><span>Our Team</span></a></li>

							<li><a href=\"".ApplicationURL($Script="whoweare")."\"><span>Who We Are</span></a></li>

							<li><a href=\"".ApplicationURL($Script="businessplan")."\"><span>Business Plan</span></a></li>-->

						</ul>

					</li>

                    <li ><a href=\"".ApplicationURL($Script="logout")."\"><span>Logout</span></a></li>

                </ul>

	";

		if ($browername == 'msie'){

	$MainContent.="

			</div>

	";

		}else{

	$MainContent.="

			</nav>

	";

		}

	}



?>