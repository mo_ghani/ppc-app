$(document).ready(function(){
	$("#formValidate").validate({
		rules: {
			UserName: {
				required: true,
				minlength:4
			},
			Password: {
				required: true
			},
			CPassword: {
				required: true,
				equalTo: "#Password"
			},
			TPassword: {
				required: true
			},
			CTPassword: {
				required: true,
				equalTo: "#TPassword"
			},
			agree: "required"
		},
		messages: {
			UserName: {
				required: "Please provide a password",
				minlength: "Atleast 4 character required"
			},
			Password: {
				required: ""
			},
			CPassword: {
				required: "Please provide a password",
				equalTo: "Please enter the same password as above"
			},
			TPassword: {
				required: ""
			},
			CTPassword: {
				required: "Please provide a password",
				equalTo: "Please enter the same password as above"
			},
			agree: "Please accept our terms and condition"
		}
	});
	
	$('#positionflag').hide();
	$(".Place").click(function()
	{
		var parent = document.getElementById('PlacementID').value;
		$('#positionflag').show();
		document.getElementById('positionflag').innerHTML = "<img src=\"./theme/default/image/ajax-loader.gif\">";
		
		$.post("./script/ajaxpositioncheck.php?",{ Place:$(this).val(),parent:parent } ,function(data)
		{
		  //if(data=='no') //if username not avaiable
		  //{
			document.getElementById('positionflag').innerHTML = data;	
		  //}
		 // else
		  //{
			//document.getElementById('positionflag').innerHTML = "<img src=\"./theme/default/image/success.gif\">";
		  //}
		});
	});
	
	$('#sponsorflag').hide();
	$("#SponsorID").blur(function()
	{
		$('#sponsorflag').show();
		document.getElementById('sponsorflag').innerHTML = "<img src=\"./theme/default/image/ajax-loader.gif\">";
		
		$.post("./script/ajaxsponorcheck.php?",{ UserName:$(this).val() } ,function(data)
		{
		  if(data=='no') //if username not avaiable
		  {
			document.getElementById('sponsorflag').innerHTML = "<img src=\"./theme/default/image/failure.gif\">";	
		  }
		  else
		  {
			document.getElementById('sponsorflag').innerHTML = "<img src=\"./theme/default/image/success.gif\">";
		  }
		});
	});
	
	$('#placementflag').hide();
	$("#PlacementID").blur(function()
	{
		$('#placementflag').show();
		document.getElementById('placementflag').innerHTML = "<img src=\"./theme/default/image/ajax-loader.gif\">";
		
		$.post("./script/ajaxparentcheck.php?",{ UserName:$(this).val() } ,function(data)
		{
		  if(data=='no') //if username not avaiable
		  {
			document.getElementById('placementflag').innerHTML = "<img src=\"./theme/default/image/failure.gif\">";	
		  }
		  else
		  {
			document.getElementById('placementflag').innerHTML = "<img src=\"./theme/default/image/success.gif\">";
		  }
		});
	});
	
	$('#userflag').hide();
	$("#UserName").blur(function()
	{
		$('#userflag').show();
		document.getElementById('userflag').innerHTML = "<img src=\"./theme/default/image/ajax-loader.gif\">";
		
		$.post("./script/ajaxusernamecheck.php?",{ UserName:$(this).val() } ,function(data)
		{
		  if(data=='no') //if username not avaiable
		  {
			document.getElementById('userflag').innerHTML = "<img src=\"./theme/default/image/failure.gif\">";	
		  }
		  else
		  {
			document.getElementById('userflag').innerHTML = "<img src=\"./theme/default/image/success.gif\">";
		  }
		});
	});

	$('#pincodeflag').hide();
	$("#PinCode").blur(function()
	{
		$('#pincodeflag').show();
		document.getElementById('pincodeflag').innerHTML = "<img src=\"./theme/default/image/ajax-loader.gif\">";
		
		$.post("./script/ajaxpincodecheck.php?",{ PinCode:$(this).val() } ,function(data)
		{
		  if(data=='no') //if username not avaiable
		  {
			document.getElementById('pincodeflag').innerHTML = "<img src=\"./theme/default/image/failure.gif\">";	
		  }
		  else
		  {
			document.getElementById('pincodeflag').innerHTML = "<img src=\"./theme/default/image/success.gif\">";
			document.getElementById('Package').value = data;
		  }
		});
	});

	
});
function agree()
	{
	 if(document.register.agree.checked==true)
	 {
	  //window.open("".ApplicationURL($Script="home")."");
	 }
	}
	
function validate()
	{
		var errorLog="";
		var errorLog2="";
		
		errorLog += validatePassword(register.Password.value);
		errorLog2 += validatePassword(register.TPassword.value);
		
		if(errorLog!="" || errorLog2!="")
		{
		document.getElementById("passwordflag").innerHTML = errorLog;
		document.getElementById("tpasswordflag").innerHTML = errorLog2;
		return false;
		}
	}
	
	function validatePassword (pw) {
	// default options (allows any password)
		var success="";
		regex = /^(?=.*[A-Z]).{6,8}$/;
		if(regex.test(pw))
		{
			return success;
		}
		else
		{
			//alert('Name can not contain numbers or symbol.');
			//myText.value = "";
			//myText.focus();
			return success = "Password must contain atleast one Capital letter and minimum 6 characters";
		}
	}


	/*function validatePassword(fld) {
		var error = "";
		var re = /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/; // allow only letters and numbers and symbols 
	 
		if(!re.test(fld.value)) {
			error = 'The password must be 8 characters that are letters, numbers and symbols (@#$%^&+=)';
			//fld.style.background = 'lightyellow';
		}
	   return error;
	} */

function passwordStrength(password)
{
	var desc = new Array();
	desc[0] = "Very Weak";
	desc[1] = "Weak";
	desc[2] = "Better";
	desc[3] = "Medium";
	desc[4] = "Strong";
	desc[5] = "Strongest";

	var score   = 0;

	//if password bigger than 4 give 1 point
	if (password.length > 4) score++;

	//if password has both lower and uppercase characters give 1 point	
	if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;

	//if password has at least one number give 1 point
	if (password.match(/\d+/)) score++;

	//if password has at least one special caracther give 1 point
	if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )	score++;

	//if password bigger than 8 give another 1 point
	if (password.length > 7) score++;

	 document.getElementById("passwordDescription").innerHTML = desc[score];
	 document.getElementById("passwordStrength").className = "strength" + score;
}

function transactionPasswordStrength(password)
{
	var desc = new Array();
	desc[0] = "Very Weak";
	desc[1] = "Weak";
	desc[2] = "Better";
	desc[3] = "Medium";
	desc[4] = "Strong";
	desc[5] = "Strongest";

	var score   = 0;

	//if password bigger than 4 give 1 point
	if (password.length > 4) score++;

	//if password has both lower and uppercase characters give 1 point	
	if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;

	//if password has at least one number give 1 point
	if (password.match(/\d+/)) score++;

	//if password has at least one special caracther give 1 point
	if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )	score++;

	//if password bigger than 8 give another 1 point
	if (password.length > 7) score++;

	 document.getElementById("passwordDescription2").innerHTML = desc[score];
	 document.getElementById("passwordStrength2").className = "strength" + score;
}