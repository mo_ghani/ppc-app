	$(document).ready(function(){
		$("#formValidate").validate({
			rules: {
				Password: {
					required: true
				},
				CPassword: {
					required: true,
					equalTo: "#Password"
				}
			},
			messages: {
				Password: {
					required: ""
				},
				CPassword: {
					required: "Please provide a password",
					equalTo: "Please enter the same password as above"
				}
			}
		});
	});
	function validate()
	{
		var errorLog="";
		
		errorLog += validatePassword(changepassword.Password.value);
		
		if(errorLog!="")
		{
		document.getElementById("passwordflag").innerHTML = errorLog;
		return false;
		}
	}

function validatePassword (pw, options) {
	// default options (allows any password)
	var success="";
	var o = {
		lower:    0,
		upper:    0,
		alpha:    1, /* lower + upper */
		numeric:  1,
		special:  1,
		length:   [8, Infinity],
		custom:   [ /* regexes and/or functions */ ],
		badWords: [],
		badSequenceLength: 0,
		noQwertySequences: false,
		noSequential:      false
	};

	for (var property in options)
		o[property] = options[property];

	var	re = {
			lower:   /[a-z]/g,
			upper:   /[A-Z]/g,
			alpha:   /[A-Z]/gi,
			numeric: /[0-9]/g,
			special: /[\W_]/g
		},
		rule, i;

	// enforce min/max length
	if (pw.length < o.length[0] || pw.length > o.length[1])
		return success="The length of password should not be more than 8 characters";

	// enforce lower/upper/alpha/numeric/special rules
	for (rule in re) {
		if ((pw.match(re[rule]) || []).length < o[rule])
			return success="Password should be mixture of letters, numbers and symbols";
	}

	// enforce word ban (case insensitive)
	for (i = 0; i < o.badWords.length; i++) {
		if (pw.toLowerCase().indexOf(o.badWords[i].toLowerCase()) > -1)
			return success="word banned";
	}

	// enforce the no sequential, identical characters rule
	if (o.noSequential && /([\S\s])\1/.test(pw))
		return success="no sequential";

	// enforce alphanumeric/qwerty sequence ban rules
	if (o.badSequenceLength) {
		var	lower   = "abcdefghijklmnopqrstuvwxyz",
			upper   = lower.toUpperCase(),
			numbers = "0123456789",
			qwerty  = "qwertyuiopasdfghjklzxcvbnm",
			start   = o.badSequenceLength - 1,
			seq     = "_" + pw.slice(0, start);
		for (i = start; i < pw.length; i++) {
			seq = seq.slice(1) + pw.charAt(i);
			if (
				lower.indexOf(seq)   > -1 ||
				upper.indexOf(seq)   > -1 ||
				numbers.indexOf(seq) > -1 ||
				(o.noQwertySequences && qwerty.indexOf(seq) > -1)
			) {
				return success="alphanumeric/qwerty sequence ban";
			}
		}
	}

	// enforce custom regex/function rules
	for (i = 0; i < o.custom.length; i++) {
		rule = o.custom[i];
		if (rule instanceof RegExp) {
			if (!rule.test(pw))
				return false;
		} else if (rule instanceof Function) {
			if (!rule(pw))
				return success="custom regex/function rules";
		}
	}

	// great success!
	return success;
}


	/*function validatePassword(fld) {
		var error = "";
		var re = /^.*(?=[-_a-zA-Z0-9]*?[A-Z])(?=[-_a-zA-Z0-9]*?[a-z])(?=[-_a-zA-Z0-9]*?[0-9])[-_a-zA-Z0-9]{6,}\z;
		var re = /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/; // allow only letters and numbers and symbols 
	 
		if(!re.test(fld.value)) {
			error = 'The password must be 8 characters that are letters, numbers and symbols (@#$%^&+=)';
			//fld.style.background = 'lightyellow';
		}
	   return error;
	}*/  

	
	function passwordStrength(password)
	{
		var desc = new Array();
		desc[0] = "Very Weak";
		desc[1] = "Weak";
		desc[2] = "Better";
		desc[3] = "Medium";
		desc[4] = "Strong";
		desc[5] = "Strongest";
	
		var score   = 0;
	
		//if password bigger than 4 give 1 point
		if (password.length > 4) score++;
	
		//if password has both lower and uppercase characters give 1 point	
		if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;
	
		//if password has at least one number give 1 point
		if (password.match(/\d+/)) score++;
	
		//if password has at least one special caracther give 1 point
		if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )	score++;
	
		//if password bigger than 8 give another 1 point
		if (password.length > 7) score++;
	
		 document.getElementById("passwordDescription").innerHTML = desc[score];
		 document.getElementById("passwordStrength").className = "strength" + score;
	}