<?
    $Entity="UserType";
    $EntityAlias="UT";
    $EntityLower=strtolower($Entity);
    $EntityCaption="User Type";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"]))$UpdateMode=true;

    $ErrorUserInput["_Error"]=false;
    CheckRequiredFormVariables(
		$Variable=array(
			array("Name"=>"UserTypeName", "Message"=>"Please provide with the user type name.")
		)
	);

    if($ErrorUserInput["_Error"]){
        include "./script/".$EntityLower."insertupdate.php";
	}else{
	    $Where="";
	    if($UpdateMode)$Where="{$Entity}ID = {$_REQUEST["UserTypeID"]} AND {$Entity}UUID = '{$_REQUEST["UserTypeUUID"]}'";

		$_POST["UserTypePicture"]=ProcessUpload("UserTypePicture", $Application["UploadPath"]);

	    $UserType=SQL_InsertUpdate(
	        $Entity,
	        $EntityAlias,
			$UserTypeData=array(
			    "UserTypeName"=>$_POST["UserTypeName"],
			    "UserTypeDescription"=>$_POST["UserTypeDescription"],
			    "UserTypePicture"=>$_POST["UserTypePicture"],
			    "UserTypeIsActive"=>$_POST["UserTypeIsActive"]
		),
			$Where
		);

	    $MainContent.="
	        ".CTL_Window($Title="Item management", "The operation complete successfully and<br>
			<br>
			the $EntityCaptionLower information has been stored.<br>
			<br>
			Please click <a href=\"".ApplicationURL($Script=$EntityLower."manage")."\">here</a> to proceed.")."
	        <script language=\"JavaScript\">
	        <!--
	            window.location='".ApplicationURL($Script=$EntityLower."manage")."';
	        -->
	        </script>
		";
	}
?>