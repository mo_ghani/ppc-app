<?php
include("db.php");

connect_my_database();

ini_set('date.timezone', 'Asia/Dhaka');

$today=date('Y-m-d');

function logger($msg){

	if ($fp = fopen('callogfile.log', 'a')) {    // Open and write to mailcetner log file

        $message=date('d-m-Y,h.i.s A').": ".$msg."\n";

        fwrite($fp, $message);

        fclose($fp);

 	}

}

logger("-------------------Incentive Calculation Start----------------------");

$qryR = "select incid,incname,investneed,achievementtime from tblincentive";
$rowarray[] = array();
$cnt = 1;
if(!$rsR = mysql_query($qryR)){

	$em = "Error in rank query: ".mysql_error();

	logger($em);

	die();
}else{
	while($rowR=mysql_fetch_array($rsR)){
		$rowarray[$cnt]['incid'] = $rowR['incid'];
		$rowarray[$cnt]['incname'] = $rowR['incname'];
		$rowarray[$cnt]['investneed'] = $rowR['investneed'];
		$rowarray[$cnt]['achievementtime'] = $rowR['achievementtime'];
		$cnt = $cnt + 1;
	} 
}

/*array testing

for ($i=1;$i<=$cnt;$i++){
	echo $rowarray[$i]['incname']."<br>";
	echo $rowarray[$i]['investneed']."<br>";
}*/
//print_r ($rowarray);

$qryInc = "SELECT UserID,RankId,InvLeftMonthly,InvRightMonthly,IncEffectiveDate FROM tblmemberstatus WHERE
IncEffectiveDate =  '". $today ."'";
if (!$rsC = mysql_query($qryInc)){

	$errmsg = "Error in query: ".mysql_error();

	logger($errmsg);

	die();

}else{

	mysql_query("BEGIN");
	
	$newdate = strtotime ( "+1 month" , strtotime ( $today ) ) ;

	$effectiveDate = date ( 'Y-m-d' , $newdate );
	//$rank = 1;
	while ($rowInc = mysql_fetch_array($rsC)){
		$UserID = $rowInc["UserID"];
		$RankId = $rowInc["RankId"];
		$InvLeftMonthly = $rowInc["InvLeftMonthly"];
		$InvRightMonthly = $rowInc["InvRightMonthly"];
		//$rowarray[$RankId]['investneed'];
		
		if ($InvLeftMonthly >= $rowarray[$RankId]['investneed'] && $InvRightMonthly >= $rowarray[$RankId]['investneed']){
			//echo "get incentive\n ";
			$RankId = $RankId + 1;
			$updRank = "update tblmemberstatus set RankId=$RankId,IncEffectiveDate='". $effectiveDate ."' where UserID = $UserID";
			if (!mysql_query($updRank)){
				$errmsg="Update Rank Failed: ".mysql_error();
				logger($errmsg); die();
			}
			$insertQry = "insert into tblmemberachievement(UserID,Date,RankID,DateInserted) values($UserID,'". $today ."',$RankId,now())";
			if(!mysql_query($insertQry)){
				$errmsg="Insert achievemnet Failed: ".mysql_error();
				logger($errmsg); die();
			}
		}else{
			$updRank = "update tblmemberstatus set IncEffectiveDate='". $effectiveDate ."',InvLeftMonthly=0,InvRightMonthly=0 where UserID = $UserID";
			if (!mysql_query($updRank)){
				$errmsg="Update monthly inv Failed: ".mysql_error();
				logger($errmsg); die();
			}
		}
		
	}
	mysql_query("COMMIT");
}

logger("-------------------Incentive Calculation End------------------------");
?>