<?
    $Entity="UserType";
    $EntityAlias="UT";
    $EntityLower=strtolower($Entity);
    $EntityCaption="User Type";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
    $FormTitle="Insert $EntityCaption";
    $ButtonCaption="Insert";
    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction");
    $UserType=array(
        "UserTypeName"=>"",
        "UserTypeDescription"=>"",
        "UserTypePicture"=>"",
        "UserTypeIsActive"=>1,
        "IsServiceProvider"=>0
	);

	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"])){
	    $UpdateMode=true;
	    $FormTitle="Update $EntityCaption";
	    $ButtonCaption="Update";
	    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction", $Entity."ID={$_REQUEST[$Entity."ID"]}&".$Entity."UUID={$_REQUEST[$Entity."UUID"]}");

		if($UpdateMode&&!isset($_POST["".$Entity."Name"]))$UserType=SQL_Select($Entity="UserType", $Where="{$EntityAlias}.{$Entity}ID = {$_REQUEST[$Entity."ID"]} AND {$EntityAlias}.{$Entity}UUID = '{$_REQUEST[$Entity."UUID"]}'", $OrderBy="{$EntityAlias}.{$Entity}Name", $SingleRow=true);
	}

	$Input=array();
    $Input[]=array("VariableName"=>"UserTypeName", "DefaultValue"=>$UserType["UserTypeName"], "Caption"=>"Type", "ControlHTML"=>CTL_InputText("UserTypeName", $UserType["UserTypeName"], "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"", "DefaultValue"=>"", "Caption"=>"", "ControlHTML"=>"<a href=\"".ApplicationURL($Script="imagestorebrowser")."\">Image Browser</a>", "Required"=>false);
    $Input[]=array("VariableName"=>"UserTypeDescription", "DefaultValue"=>$UserType["UserTypeDescription"], "Caption"=>"Description", "ControlHTML"=>CTL_InputTextArea($Name="UserTypeDescription", $Value=$UserType["UserTypeDescription"], $Columns=89, $Rows=3), "Required"=>false);
    $Input[]=array("VariableName"=>"UserTypePicture", "DefaultValue"=>$UserType["UserTypePicture"], "Caption"=>"Image", "ControlHTML"=>CTL_ImageUpload($ControlName="UserTypePicture", $CurrentImage=$UserType["UserTypePicture"], $AllowDelete=$UpdateMode, $Class="FormTextInput", $ThumbnailHeight=100, $ThumbnailWidth=0, $Preview=$UpdateMode), "Required"=>false);
    $Input[]=array("VariableName"=>"UserTypeIsActive", "DefaultValue"=>$UserType["UserTypeIsActive"], "Caption"=>"Active?", "ControlHTML"=>CTL_InputRadioSet($VariableName="UserTypeIsActive", $Captions=array("Yes", "No"), $Values=array(1, 0), $CurrentValue=$UserType["UserTypeIsActive"]), "Required"=>false);

	$MainContent.=FormInsertUpdate(
		$EntityName=$EntityLower,
		$FormTitle,
		$Input,
		$ButtonCaption,
		$ActionURL
	);
?>