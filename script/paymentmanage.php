<?
    $Entity="Payment";
    $EntityLower=strtolower($Entity);
    $EntityCaption="Payment";
    $EntityCaptionLower=strtolower($EntityCaption);

    SetFormvariable("RecordShowFrom", 1);
    SetFormvariable("RecordShowUpTo", 20);
    SetFormvariable("SortBy", "DateInserted");
    SetFormvariable("SortType", "DESC");

    $ControlHTML="";

    $SearchHTML="
		Search ".CTL_InputText($Name="FreeText", $DefaultValue="", $Title="", $Size="35")."
	";

    $Where="U.UserTypeID != 1 AND U.UserTypeID != 2";
	if($_POST["FreeText"]!="")$Where.=" AND U.UserName LIKE '%{$_POST["FreeText"]}%'";
	
	$MainContent.=CTL_Datagrid(
		$Entity,
		$ColumnName=array("UserName", "TotalAmount", "Chrgd", "Balance"),
		$ColumnTitle=array("Member ID", "Total Amount", "Service Charge", "Balance"),
		$ColumnAlign=array("left", "left", "left", "left"),
		$ColumnType=array("text", "text", "text", "text"),
		$Rows=SQL_Select($Entity="Payment", $Where, $OrderBy="{$_REQUEST["SortBy"]} {$_REQUEST["SortType"]}", $SingleRow=false, $RecordShowFrom=$_POST["RecordShowFrom"], $RecordShowUpTo=$_POST["RecordShowUpTo"], $Debug=false),
		$SearchHTML,
		$ControlHTML,
		$AdditionalLinks=array(array("Action"=>"paymentupdate", "Parameter"=>"", "Tooltip"=>"Update", "Image"=>"edit")),
		$AdditionalActionParameter="",
		$ActionLinks=false,
		$SearchPanel=true,
		$ControlPanel=false,
		$CheckBox=false,
		$EntityAlias="".$EntityCaption."",
		$SortLinkExtraParameter="",
		$ControlPanelFormActionExtraParameter=""
	);

?>