<?
	$Entity="User";
    $EntityAlias="U";
    $EntityLower=strtolower($Entity);
    $EntityCaption="User";
    $EntityCaptionLower=strtolower($EntityCaption);

    SetFormvariable("RecordShowFrom", 1);
    SetFormvariable("RecordShowUpTo", $Application["DatagridRowsDefault"]);
    SetFormvariable("SortBy", "DateInserted");
    SetFormvariable("SortType", "DESC");

	if(isset($_POST["ActionNew{$Entity}"]))include "./script/".$EntityLower."insertupdate.php";

    $ControlHTML="
		".CTL_InputSubmit($Name="ActionNew{$Entity}", $Value="New {$EntityCaption}")."
	";

    $SearchHTML="
		Search ".CTL_InputText($Name="FreeText", $DefaultValue="", $Title="", $Size="35")."
	";

    $Where="U.UserTypeID != 1 AND U.UserTypeID != 2";
	//Allow administrator to list all the users available in the system
   // if($_SESSION["UserTypeID"]==$Application["UserTypeIDAdministrator"])$Where="1 = 1";
	//Apply the free text search
	if($_POST["FreeText"]!="")$Where.=" AND (U.UserName LIKE '%{$_POST["FreeText"]}%' OR U.Name LIKE '%{$_POST["FreeText"]}%' OR U.City LIKE '%{$_POST["FreeText"]}%' OR U.PostCode LIKE '%{$_POST["FreeText"]}%' OR U.State LIKE '%{$_POST["FreeText"]}%')";

	$MainContent.=CTL_Datagrid(
		$Entity,
		$ColumnName=array("{$Entity}Email", "{$Entity}Name", "Name", "{$Entity}IsActive", "DateInserted"),
		$ColumnTitle=array("", "User ID", "Full Name", "Active?", "Joined"),
		$ColumnAlign=array("center", "left", "left", "left", "left"),
		$ColumnType=array("email", "text", "text", "yes/no", "date"),
		$Rows=SQL_Select($Entity="User", $Where, $OrderBy="{$_REQUEST["SortBy"]} {$_REQUEST["SortType"]}", $SingleRow=false, $RecordShowFrom=$_POST["RecordShowFrom"], $RecordShowUpTo=$_POST["RecordShowUpTo"], $Debug=false),
		$SearchHTML,
		$ControlHTML,
		$AdditionalLinks=array(array("Action"=>"memberprofile", "Parameter"=>"", "Tooltip"=>"Update", "Image"=>"edit")),
		$AdditionalActionParameter="",
		$ActionLinks=false,
		$SearchPanel=true,
		$ControlPanel=false,
		$CheckBox=false,
		$EntityAlias="".$EntityCaption."",
		$SortLinkExtraParameter="",
		$ControlPanelFormActionExtraParameter=""
	);
?>