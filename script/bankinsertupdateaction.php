<?
    $Entity="Bank";
    $EntityAlias="B";
    $EntityLower=strtolower($Entity);
    $EntityCaption="Bank";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"]))$UpdateMode=true;

    $ErrorUserInput["_Error"]=false;
    CheckRequiredFormVariables(
		$Variable=array(
			array("Name"=>"BankName", "Message"=>"Please provide with the Bank Name.")
		)
	);

    if($ErrorUserInput["_Error"]){
        include "./script/".$EntityLower."insertupdate.php";
	}else{
	    $Where="";
	    if($UpdateMode)$Where="{$Entity}ID = {$_REQUEST["BankID"]} AND {$Entity}UUID = '{$_REQUEST["BankUUID"]}'";

//		$_POST["JobSubCategoryPicture"]=ProcessUpload("JobSubCategoryPicture", $Application["UploadPath"]);

	    $Bank=SQL_InsertUpdate(
	        $Entity,
	        $EntityAlias,
			$BankData=array(
			    "BankName"=>$_POST["BankName"],
				"BankCountryID"=>$_POST["CountryID"]
		),
			$Where
		);

	    $MainContent.="
	        ".CTL_Window($Title="Item management", "The operation complete successfully and<br>
			<br>
			the $EntityCaptionLower information has been stored.<br>
			<br>
			Please click <a href=\"".ApplicationURL($Script=$EntityLower."manage")."\">here</a> to proceed.")."
	        <script language=\"JavaScript\">
	        <!--
	            window.location='".ApplicationURL($Script=$EntityLower."manage")."';
	        -->
	        </script>
		";
	}
?>