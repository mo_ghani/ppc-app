<?
    $Entity="Tours";
    $EntityAlias="T";
    $EntityLower=strtolower($Entity);
    $EntityCaption="Tours";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"]))$UpdateMode=true;

    $ErrorUserInput["_Error"]=false;
    CheckRequiredFormVariables(
		$Variable=array(
			array("Name"=>"ToursName", "Message"=>"Please provide with the Name."),
			array("Name"=>"ToursPrice", "Message"=>"Please provide with the Price."),
			array("Name"=>"ToursDescription", "Message"=>"Please provide with the Description.")
		)
	);

    if($ErrorUserInput["_Error"]){
        include "./script/".$EntityLower."insertupdate.php";
	}else{
	    $Where="";
	    if($UpdateMode)$Where="{$Entity}ID = {$_REQUEST["ToursID"]} AND {$Entity}UUID = '{$_REQUEST["ToursUUID"]}'";

		$_POST["ToursPic"]=ProcessUpload("ToursPic", $Application["UploadPath"]);

	    $Tours=SQL_InsertUpdate(
	        $Entity,
	        $EntityAlias,
			$ToursData=array(
			    "ToursName"=>$_POST["ToursName"],
				"ToursPrice"=>$_POST["ToursPrice"],
				"ToursDescription"=>$_POST["ToursDescription"],
				"ToursPic"=>$_POST["ToursPic"]
		),
			$Where
		);

	    $MainContent.="
	        ".CTL_Window($Title="Item management", "The operation complete successfully and<br>
			<br>
			the $EntityCaptionLower information has been stored.<br>
			<br>
			Please click <a href=\"".ApplicationURL($Script=$EntityLower."manage")."\">here</a> to proceed.")."
	        <script language=\"JavaScript\">
	        <!--
	            window.location='".ApplicationURL($Script=$EntityLower."manage")."';
	        -->
	        </script>
		";
	}
?>