<?
    $Entity="ApplicationSetting";
    $EntityAlias="APS";
    $EntityLower=strtolower($Entity);
    $EntityCaption="Application Setting";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"]))$UpdateMode=true;

    $ErrorUserInput["_Error"]=false;
    CheckRequiredFormVariables(
		$Variable=array(
			array("Name"=>"ApplicationSettingName", "Message"=>"Please provide with the application setting name."),
			array("Name"=>"ApplicationSettingValue", "Message"=>"Please provide with the application setting value."),
			array("Name"=>"ApplicationSettingInputTypeName", "Message"=>"Please provide with the application setting type name.")
		)
	);

    if($ErrorUserInput["_Error"]){
        include "./script/".$EntityLower."insertupdate.php";
	}else{
	    $Where="";
	    if($UpdateMode)$Where="{$Entity}ID = {$_REQUEST["UserTypeID"]} AND {$Entity}UUID = '{$_REQUEST["UserTypeUUID"]}'";

		//$_POST["UserTypePicture"]=ProcessUpload("UserTypePicture", $Application["UploadPath"]);

	    $UserType=SQL_InsertUpdate(
	        $Entity,
	        $EntityAlias,
			$UserTypeData=array(
			    "ApplicationSettingName"=>$_POST["ApplicationSettingName"],
			    "ApplicationSettingValue"=>$_POST["ApplicationSettingValue"],
			    "ApplicationSettingDescription"=>$_POST["ApplicationSettingDescription"],
			    "ApplicationSettingInputTypeName"=>$_POST["ApplicationSettingInputTypeName"],
			    "ApplicationSettingIsHidden"=>$_POST["ApplicationSettingIsHidden"],
			    "ApplicationSettingIsLocked"=>$_POST["ApplicationSettingIsLocked"],
			    "ApplicationSettingIsActive"=>$_POST["ApplicationSettingIsActive"]
		),
			$Where
		);

	    $MainContent.="
	        ".CTL_Window($Title="$EntityCaption management", "The operation complete successfully and<br>
			<br>
			the $EntityCaptionLower information has been stored.<br>
			<br>
			Please click <a href=\"".ApplicationURL($Script=$EntityLower."manage")."\">here</a> to proceed.")."
	        <script language=\"JavaScript\">
	        <!--
	            window.location='".ApplicationURL($Script=$EntityLower."manage")."';
	        -->
	        </script>
		";
	}

?>