<?
	/*
		Script:		
		Author:		Shahriar Kabir (SKJoy2001@Yahoo.Com)
		Date:		
		Purpose:	
		Note:		
	*/
	
	$UpdateMode=false;

    $StaticContent=SQL_Select($Entity="StaticContent", $Where="SC.StaticContentName = '{$_REQUEST["StaticContentName"]}' AND L.LanguageCode = '{$_REQUEST["LanguageCode"]}'", $OrderBy="SC.StaticContentName", $SingleRow=true, $RecordShowFrom=0, $RecordShowUpTo=0, $Debug=false);
	if(count($StaticContent)>1)$UpdateMode=true;

    $Language=SQL_Select($Entity="Language", $Where="L.LanguageCode = '{$_REQUEST["LanguageCode"]}'", $OrderBy="L.LanguageName", $SingleRow=true, $RecordShowFrom=0, $RecordShowUpTo=0, $Debug=false);
	$StaticContentData=array(
	    "StaticContentName"=>$_REQUEST["StaticContentName"],
	    "StaticContent"=>$_POST["StaticContent"],
	    "LanguageID"=>$Language["LanguageID"],
	    "StaticContentPicture"=>$StaticContent["StaticContentPicture"],
	    "StaticContentIsActive"=>1
	);

    $Where="";
	if($UpdateMode)$Where="StaticContentName = '{$_REQUEST["StaticContentName"]}' AND LanguageID = '{$Language["LanguageID"]}'";

	SQL_InsertUpdate($Entity="StaticContent", $EntityAlias="SC", $StaticContentData, $Where);

	$MainContent.="
	    ".CTL_Window(
			$Title="Content manager",
			$Content="
			    The content has successfully been stored. Please click <a href=\"#\" onclick=\"window.close()\">here</a> to proceed.
			"
		)."
		
		<script language=\"JavaScript\">
		<!--
		    window.close();
		-->
		</script>
	";
?>