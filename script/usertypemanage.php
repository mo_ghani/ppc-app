<?
    $Entity="UserType";
    $EntityAlias="UT";
    $EntityLower=strtolower($Entity);
    $EntityCaption="User Type";
    $EntityCaptionLower=strtolower($EntityCaption);

	SetFormvariable("RecordShowFrom", 1);
    SetFormvariable("RecordShowUpTo", $Application["DatagridRowsDefault"]);
    SetFormvariable("SortBy", "UserTypeName");
    SetFormvariable("SortType", "ASC");

	if(isset($_POST["ActionNew{$Entity}"]))include "./script/".$EntityLower."insertupdate.php";

    $ControlHTML="
		".CTL_InputSubmit($Name="ActionNew{$Entity}", $Value="New1 {$EntityCaption}")."
	";

    $SearchHTML="
		Free text ".CTL_InputText($Name="FreeText")."
	";

    $Where="UT.UserTypeID NOT IN ({$Application["UserTypeIDGuest"]}, {$Application["UserTypeIDAdministrator"]})";
	if($_POST["FreeText"]!="")$Where.=" AND {$EntityAlias}.{$Entity}Name LIKE '%{$_POST["FreeText"]}%'";

	$MainContent.= CTL_Datagrid(
		$Entity,
		$ColumnName=array("{$Entity}Picture", "{$Entity}Name", "{$Entity}IsActive", "DateInserted"),
		$ColumnTitle=array("", "Type", "Active?", "Inserted"),
		$ColumnAlign=array("center", "left", "left", "left"),
		$ColumnType=array("imagelink", "text", "yes/no", "date"),
		$Rows=SQL_Select($Entity="UserType", $Where, $OrderBy="{$_REQUEST["SortBy"]} {$_REQUEST["SortType"]}", $SingleRow=false, $RecordShowFrom=$_POST["RecordShowFrom"], $RecordShowUpTo=$_POST["RecordShowUpTo"], $Debug=false),
		$SearchHTML,
		$ControlHTML,
		$AdditionalLinks=array(array("Action"=>"usertypeinsertupdate", "Parameter"=>"UseTinyMCE", "Tooltip"=>"Send", "Image"=>"email")),
		$AdditionalActionParameter="",
		$ActionLinks=true,
		$SearchPanel=true,
		$ControlPanel=true,
		$CheckBox=false,
		$EntityAlias="".$EntityCaption."",
		$SortLinkExtraParameter="",
		$ControlPanelFormActionExtraParameter=""
	);
?>