<?
    $Entity="Bank";
    $EntityAlias="B";
    $EntityLower=strtolower($Entity);
    $EntityCaption="Bank";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
    $FormTitle="Insert $EntityCaption";
    $ButtonCaption="Insert";
    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction");
    $Bank=array(
        "BankName"=>""
	);

	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"])){
	    $UpdateMode=true;
	    $FormTitle="Update $EntityCaption";
	    $ButtonCaption="Update";
	    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction", $Entity."ID={$_REQUEST[$Entity."ID"]}&".$Entity."UUID={$_REQUEST[$Entity."UUID"]}");

		if($UpdateMode&&!isset($_POST["".$Entity."Name"]))$Bank=SQL_Select($Entity="Bank", $Where="{$EntityAlias}.{$Entity}ID = {$_REQUEST[$Entity."ID"]} AND {$EntityAlias}.{$Entity}UUID = '{$_REQUEST[$Entity."UUID"]}'", $OrderBy="{$EntityAlias}.{$Entity}Name", $SingleRow=true);
	}

	$Input=array();
    $Input[]=array("VariableName"=>"CountryID", "DefaultValue"=>$Bank["CountryID"], "Caption"=>"Country", "ControlHTML"=>CCTL_CountryLookup($Name="CountryID", $ValueSelected=$Bank["BankCountryID"]), "Required"=>false);
    $Input[]=array("VariableName"=>"BankName", "DefaultValue"=>$Bank["BankName"], "Caption"=>"Bank Name", "ControlHTML"=>CTL_InputText("BankName", $Bank["BankName"], "", 61), "Required"=>true);

	$MainContent.=FormInsertUpdate(
		$EntityName=$EntityLower,
		$FormTitle,
		$Input,
		$ButtonCaption,
		$ActionURL
	);
?>