<?

	$userQuery=mysql_query("Select * from tbluser Where UserID='".decrypt($_REQUEST["uid"])."'");

	$rowUser=mysql_fetch_array($userQuery);



	$countryQuery=mysql_query("Select CountryName from tblcountry Where CountryID='".$rowUser["CountryID"]."'");

	$rowCountry=mysql_fetch_array($countryQuery);



	$memberQuery=mysql_query("Select * from tblmemberstatus Where UserID='".$rowUser["UserID"]."'");

	$rowMemember=mysql_fetch_array($memberQuery);



	$rankQuery=mysql_query("Select RankName,InterestRate from tblrank Where RankID='".$rowMemember["RankId"]."'");

	$rowRank=mysql_fetch_array($rankQuery);



	$investQuery=mysql_query("Select * from tblinvestment Where InvId='".decrypt($_REQUEST["id"])."'");

	$rowInvest=mysql_fetch_array($investQuery);



	$today=date('Y-m-d');

	$dateToday = date('jS F, Y', strtotime($today));

	$InvestmentDate = date('jS F, Y', strtotime($rowInvest["InvestmentDate"]));

	if ($rowInvest["MatureDate"]=="1970-01-01"){
		$MatureDate = "-";
	}else{
		$MatureDate = date('jS F, Y', strtotime($rowInvest["MatureDate"]));
	}
   //$MatureDate = date('jS F, Y', strtotime($rowInvest["MatureDate"]));


	$MainContent.="

	<title>Invoice of {$rowUser["Name"]}</title>

	<link rel=\"stylesheet\" type=\"text/css\" href=\"./theme/{$_REQUEST["Theme"]}/style/commonSt.css\">

	<table style=\"BORDER-COLLAPSE: collapse;\" borderColor=\"silver\" cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">

		<tr>

			<td>

				<table cellSpacing=\"0\" cellPadding=\"4\" width=\"100%\" border=\"0\">

					<tr>

						<td colSpan=\"2\">

							<div id=\"strid\" align=\"right\"><INPUT class=\"btnclass\" onclick=\"Javascript:window.print();\"  type=\"button\" value=\"Print Report\" name=\"button\">

								<INPUT class=\"btnclass\" onclick=\"Javascript:self.close()\" type=\"button\" value=\"Close\" name=\"button\">

							</div>

						</td>

					</tr>

					<tr>

						<td width=\"100%\" colSpan=\"2\" class=\"cls800\" bgcolor=\"#ffffff\"><DIV align=\"left\"><img src=\"./theme/default/image/logo.png\">&nbsp;</DIV><hr></td>

					</tr>

					<tr>

						<td colSpan=\"2\">&nbsp;</td>

					</tr>

					<tr>

						<td colSpan=\"2\"><div align=\"right\"><span id=\"lblstatus\" class=\"invhead\"></span></div>

						</td>

					</tr>

					<tr>

						<td class=\"invhead1\" align=\"left\" width=\"30%\"><DIV align=\"left\"><b>Bill to</b>&nbsp;</DIV>

						</td>

						<td align=\"right\" width=\"70%\">

							<DIV align=\"right\">PROFORMA INVOICE&nbsp;<br><span id=\"lblvatno\"></span></DIV>

						</td>

					</tr>

					<tr>

						<td colSpan=\"2\">

							<table cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">

								<tr>

									<td class=\"invaddress\" style=\"WIDTH: 725px\" vAlign=\"top\" align=\"left\" width=\"725\">

									<span id=\"lbladdress\">

									<font size=2><b>{$rowUser["Name"]}</b></font>,

									<br>

									MEMBER ID:&nbsp;{$rowUser["UserName"]}</span>

									</td>

									<td class=\"invaddress\" align=\"right\" width=\"45%\">

										<table style=\"BORDER-COLLAPSE: collapse\" borderColor=\"silver\" cellSpacing=\"0\" cellPadding=\"4\" width=\"100%\" border=\"0\">

											<tr>

												<td class=\"invbottomright\" style=\"WIDTH: 154px\" align=\"right\">

													<div align=\"right\">Date&nbsp;</div>

												</td>

												<td class=\"invbottom\" align=\"left\"><span id=\"lbldate\">{$dateToday}</span></td>

											</tr>

											<tr>

												<td class=\"invbottomright\" style=\"WIDTH: 154px\" align=\"right\">

													<div align=\"right\">Start Date&nbsp;</div>

												</td>

												<td class=\"invbottom\" align=\"left\"><span id=\"lblstartdate\">{$InvestmentDate}</span></td>

											</tr>

											<tr>

												<td class=\"invbottomright\" style=\"WIDTH: 154px\" align=\"right\">

													<div align=\"right\">Last Date&nbsp;</div>

												</td>

												<td class=\"invbottom\" align=\"left\"><span id=\"lblenddate\">{$MatureDate}</span></td>

											</tr>

											<tr>

												<td class=\"invbottomright\" style=\"WIDTH: 154px\" align=\"right\">

													<div align=\"right\">Country&nbsp;</div>

												</td>

												<td class=\"invbottom\" align=\"left\">

													<span id=\"lblcountry\">{$rowCountry["CountryName"]}</span>

												</td>

											</tr>

										</table>

									</td>

								</tr>

							</table>

						</td>

					</tr>

					<tr>

						<td colSpan=\"2\">&nbsp;</td>

					</tr>

					<tr>

						<td align=\"left\" colSpan=\"2\">

							<table cellSpacing=\"0\" cellPadding=\"3\" width=\"100%\" border=\"0\">

								<TR>

									<TD class=\"invtable\" width=\"10%\">ITEM</TD>

									<TD class=\"invtable\" width=\"65%\">Package</TD>

									<TD class=\"invtable\" width=\"25%\">

										<DIV align=\"right\">AMOUNT&nbsp;</DIV>

									</TD>

								</TR>

								<tr>

									<td class=\"invtext\" vAlign=\"top\" width=\"10%\">1.</td>

									<td class=\"invtext\" vAlign=\"top\" width=\"65%\"><span id=\"lbldescription\">{$rowRank["RankName"]}</span></td>

									<td class=\"invtext\" vAlign=\"top\" width=\"25%\">

										<div align=\"right\"><span id=\"lblamount\">UTA&nbsp;{$rowInvest["Investment"]}</span>&nbsp;</div>

									</td>

								</tr>

							</table>

						</td>

					</tr>

					<tr>

						<td colSpan=\"2\">&nbsp;</td>

					</tr>

					<tr>

						<td class=\"invtext\" align=\"left\" colSpan=\"2\"><b>Terms and Conditions</b></td>

					</tr>

					<tr>

						<td class=\"invtext\" align=\"left\" colSpan=\"2\">

							<UL>

								<LI>

									We undertake to remunerate <!--a total of

									<strong>{$rowRank["InterestRate"]}%</strong> rebate (including principal amount) per month of

									the total purchase price of purchased Investment Package to the above mentioned purchaser,-->

									for 10 months commencing from the start date and ending 10 months from the start date.<BR>

									<BR>

								<LI>

									All transactions shall be carried out in USTOURISMS Website, in the manner and procedures as shall be

									prescribed by us from time to time. Your rebates shall be transfered to your

									individual trading account from which you may request a withdrawal.

									<BR>

									<BR>

								<LI>

									<B>We do not undertake to honor this transaction in any other method, way or form.</B><BR>

									<BR>

								<LI>

									<B>www.USTOURISMS.com powered by Concord Resort LLC.</B><BR>

									<BR>

								</LI>

							</UL>

						</td>

					</tr>

					<tr>

						<td colSpan=\"2\">&nbsp;</td>

					</tr>

					<tr>

						<td class=\"invtext\">&nbsp;(E &amp; O.E)</td>

						<td>

							<div align=\"right\"><font class=\"invtotal\">TOTAL:&nbsp;<span id=\"lbltotal\">UTA&nbsp;{$rowInvest["Investment"]}</span></font>&nbsp;</div>

						</td>

					</tr>

					<tr>

						<td colSpan=\"2\">&nbsp;</td>

					</tr>

					<tr>

						<td class=\"invtext\" colspan=\"2\">

							<table cellSpacing=\"0\" cellPadding=\"3\" width=\"100%\" border=\"0\">

								<tr>

									<td class=\"invtext\">

									<img src=\"./theme/default/image/smith 1.png\" width=\"127\" >&nbsp;<BR>

										<br>

									<strong style=\"font-size:16px;\">CEO, USTOURISMS</strong>

									</td>

								</tr>

							</table>

						</td>

					</tr>

				</table>

			</td>

		</tr>

   </table>

	";

?>