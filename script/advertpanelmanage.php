<?
    $Entity="AdvertPanel";
    $EntityAlias="AP";
    $EntityLower=strtolower($Entity);
    $EntityCaption="Advert Panel";
    $EntityCaptionLower=strtolower($EntityCaption);

	SetFormvariable("RecordShowFrom", 1);
    SetFormvariable("RecordShowUpTo", $Application["DatagridRowsDefault"]);
    SetFormvariable("SortBy", "AdvertPanelName");
    SetFormvariable("SortType", "ASC");

	if(isset($_POST["ActionNew{$Entity}"]))include "./script/".$EntityLower."insertupdate.php";

    $ControlHTML="
		".CTL_InputSubmit($Name="ActionNew{$Entity}", $Value="New {$EntityCaption}")."
	";

    $SearchHTML="
		Free text ".CTL_InputText($Name="FreeText")."
	";

    $Where="1 = 1";
	if($_POST["FreeText"]!="")$Where.=" AND {$EntityAlias}.{$Entity}Name LIKE '%{$_POST["FreeText"]}%'";

	$MainContent.= CTL_Datagrid(
		$Entity,
		$ColumnName=array("{$Entity}Name", "{$Entity}IsVertical", "{$Entity}IsActive"),
		$ColumnTitle=array("Name", "Vertical?", "Active?"),
		$ColumnAlign=array("left", "left", "left"),
		$ColumnType=array("text", "yes/no", "yes/no"),
		$Rows=SQL_Select($Entity="AdvertPanel", $Where, $OrderBy="{$_REQUEST["SortBy"]} {$_REQUEST["SortType"]}", $SingleRow=false, $RecordShowFrom=$_POST["RecordShowFrom"], $RecordShowUpTo=$_POST["RecordShowUpTo"], $Debug=false),
		$SearchHTML,
		$ControlHTML,
		$AdditionalLinks=array(),
		$AdditionalActionParameter="",
		$ActionLinks=true,
		$SearchPanel=true,
		$ControlPanel=true,
		$CheckBox=false,
		$EntityAlias="".$EntityCaption."",
		$SortLinkExtraParameter="",
		$ControlPanelFormActionExtraParameter=""
	);
?>