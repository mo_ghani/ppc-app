<?
	
	SessionUnsetUser();

	$MainContent.="
		<table width=\"850\" border=\"0\" align=\"center\" cellpadding=\"5\">
			<tr>
				<td>
					<br><br>
					<span class=\"headerbg\">
						Login out
					</span>
					<br><br>
				</td>
			</tr>
			<tr>
				<td height=\"100%\" style=\"font-size:12px;\" valign=\"top\"><br>
					Dear user,<br>
					<br>
					You have successfully logged out of the system, please wait while we redirect you to the home page.<br>
					<br>
					{$Application["Title"]}<br>
				</td>
			</tr>
			<tr>
				<td height=\"100\"></td>
			</tr>
		</table>
		<script language=\"JavaScript\">
		<!--
		    window.location.href='".ApplicationURL()."';
		-->
		</script>
	";
?>
