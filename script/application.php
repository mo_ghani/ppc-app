<?
	//Set the default application variables
	$Application=array(
	    "Title"=>									"..::USTOURISMS::..",
	    "UseDatabase"=>								true,

	    "DatabaseServer"=>							"localhost",
	    "DatabaseName"=>							"ustourisms_new",
	    "DatabaseUsername"=>						"root",
	    "DatabasePassword"=>						"",
		
	    "DatabaseTableNamePrefix"=>					"",
	    "UploadPath"=>								"./upload/",
	    "LanguageCodeDefault"=>						"EN-US",
	    "DatagridRowsDefault"=>                     17,
	    "DaysToWaitForRegistrationConfirmation"=>	30,

	    "UserTypeIDGuest"=>							1,
	    "UserTypeNameGuest"=>						"Guest",
	    "UserTypeIDAdministrator"=>					2,
	    "UserTypeIDMember"=>						3,

	    "UserIDGuest"=>								1,
	    "UserUUIDGuest"=>							"",
	    "UserNameGuest"=>							"Guest",
	    "UserPasswordGuest"=>						"",
	    "UserEmailGuest"=>							"admin@ustourisms.come",

	    "SessionTimeout"=>							20,
	    "EmailContact"=>							"admin@ustourisms.come",
	    "EmailSupport"=>							"admin@ustourisms.come"
	);

?>