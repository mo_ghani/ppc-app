<?


    $Entity="Advert";


    $EntityAlias="A";


    $EntityLower=strtolower($Entity);


    $EntityCaption="Advert";


    $EntityCaptionLower=strtolower($EntityCaption);





    $UpdateMode=false;


    $FormTitle="Insert $EntityCaption";


    $ButtonCaption="Insert";


    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction");


    $Advert=array(


        "AdvertName"=>"",
		"AdvertPicture"=>"",
        "AdvertClickURL"=>"",
        "AdvertTime"=>"",
        "AdvertIsActive"=>1


	);





	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"])){


	    $UpdateMode=true;


	    $FormTitle="Update $EntityCaption";


	    $ButtonCaption="Update";


	    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction", $Entity."ID={$_REQUEST[$Entity."ID"]}&".$Entity."UUID={$_REQUEST[$Entity."UUID"]}");





		if($UpdateMode&&!isset($_POST["".$Entity."Name"]))$Advert=SQL_Select($Entity="Advert", $Where="{$EntityAlias}.{$Entity}ID = {$_REQUEST[$Entity."ID"]} AND {$EntityAlias}.{$Entity}UUID = '{$_REQUEST[$Entity."UUID"]}'", $OrderBy="{$EntityAlias}.{$Entity}Name", $SingleRow=true);


	}





	$Input=array();


    $Input[]=array("VariableName"=>"AdvertName", "DefaultValue"=>$Advert["AdvertName"], "Caption"=>"Name", "ControlHTML"=>CTL_InputText("AdvertName", $Advert["AdvertName"], "", 61), "Required"=>true);


   // $Input[]=array("VariableName"=>"AdvertType", "DefaultValue"=>$Advert["AdvertType"], "Caption"=>"Type", "ControlHTML"=>CTL_Combo($Name="AdvertType", $Values=array("Image", "Flash"), $Captions=array("Image", "Flash"), $IncludeBlankItem=false, $CurrentValue=$Advert["AdvertType"]), "Required"=>true);


    //$Input[]=array("VariableName"=>"AdvertPanelID", "DefaultValue"=>$Advert["AdvertPanelID"], "Caption"=>"Panel", "ControlHTML"=>CCTL_AdvertPanelLookup($Name="AdvertPanelID", $ValueSelected=0, $Where="", $PrependBlankOption=false), "Required"=>false);


    $Input[]=array("VariableName"=>"AdvertPicture", "DefaultValue"=>$Advert["AdvertPicture"], "Caption"=>"Image", "ControlHTML"=>CTL_ImageUpload($ControlName="AdvertPicture", $CurrentImage=$Advert["AdvertPicture"], $AllowDelete=$UpdateMode, $Class="FormTextInput", $ThumbnailHeight=100, $ThumbnailWidth=0, $Preview=$UpdateMode)."<br><br>", "Required"=>false);


    $Input[]=array("VariableName"=>"AdvertClickURL", "DefaultValue"=>$Advert["AdvertClickURL"], "Caption"=>"URL", "ControlHTML"=>CTL_InputText("AdvertClickURL", $Advert["AdvertClickURL"], "", 61), "Required"=>true);


    $Input[]=array("VariableName"=>"AdvertTime", "DefaultValue"=>$Advert["AdvertTime"], "Caption"=>"Count Time", "ControlHTML"=>CTL_InputText("AdvertTime", $Advert["AdvertTime"], "", 21), "Required"=>true);


    $Input[]=array("VariableName"=>"AdvertIsActive", "DefaultValue"=>$Advert["AdvertIsActive"], "Caption"=>"Active?", "ControlHTML"=>CTL_InputRadioSet($VariableName="AdvertIsActive", $Captions=array("Yes", "No"), $Values=array(1, 0), $CurrentValue=$Advert["AdvertIsActive"]), "Required"=>false);





	$MainContent.=FormInsertUpdate(


		$EntityName=$EntityLower,


		$FormTitle,


		$Input,


		$ButtonCaption,


		$ActionURL


	);


?>