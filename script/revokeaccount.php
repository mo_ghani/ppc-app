<?
	/*
		Script:		
		Author:	
		Date:		
		Purpose:	
		Note:		
	*/
	
	$MainContent.=CTL_Window(
		$Title="Revoke account",
		$Content="
			<form action=\"".ApplicationURL($Script="revokeaccountaction", "UserID=".$_SESSION["UserID"]."&UserUUID=".$_SESSION["UserUUID"]."")."\" method=\"post\">
				Are you sure you want to revoke your account?<br>
				<br>
				<div align=\"right\">
				    ".CTL_InputSubmit("", $Value="No")."
				    ".CTL_InputSubmit("DeleteConfirm", $Value="Yes")."
				</div>
			</form>
		",
		0,
		$Icon="question"
	);
?>
