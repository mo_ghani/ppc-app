<?
    $Entity="User";
    $EntityAlias="U";
    $EntityLower=strtolower($Entity);
    $EntityCaption="User";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"]))$UpdateMode=true;

    CheckRequiredFormVariables(
		$Variable=array(
			array("Name"=>"UserName", "Message"=>"Please provide with the user name."),
			array("Name"=>"UserPassword", "Message"=>"Please provide with the password."),
			array("Name"=>"UserPasswordConfirm", "Message"=>"Please confirm the password."),
			array("Name"=>"UserEmail", "Message"=>"Please provide with the email."),
			array("Name"=>"NameFirst", "Message"=>"Please provide with the first name."),
			array("Name"=>"NameLast", "Message"=>"Please provide with the last name.")
		)
	);

	if($_POST["UserPassword"]!=$_POST["UserPasswordConfirm"]){
		$ErrorUserInput["_Error"]=true;
		$ErrorUserInput["_Message"]="Please verify the password and confirm.";
	}

	if(!$UpdateMode)$_REQUEST["UserID"]=0;
	$User=SQL_Select($Entity="User", $Where="U.UserName='{$_POST["UserName"]}' AND U.UserID <> {$_REQUEST["UserID"]}");
    if(count($User)>0){
	    $ErrorUserInput["_Error"]=true;
	    $ErrorUserInput["_Message"]="Username already taken.";
	}

    if($ErrorUserInput["_Error"]){
        include "./script/".$EntityLower."insertupdate.php";
	}else{
		//$_POST["UserPicture"]=ProcessUpload("UserPicture", $Application["UploadPath"]);

	    $Where="";
	    if($UpdateMode)$Where="{$Entity}ID = {$_REQUEST["UserID"]} AND {$Entity}UUID = '{$_REQUEST["UserUUID"]}'";
		
	    $User=SQL_InsertUpdate(
			$Entity,
			$EntityAlias,
			$UserData=array(
			    "UserName"=>$_POST["UserName"],
			    "UserEmail"=>$_POST["UserEmail"],
			    "UserDescription"=>$_POST["UserDescription"],
			    "UserPassword"=>$_POST["UserPassword"],
			    "UserTypeID"=>$_POST["UserTypeID"],
			    "UserIsActive"=>$_POST["UserIsActive"],
			    "NameFirst"=>$_POST["NameFirst"],
			    "NameMiddle"=>$_POST["NameMiddle"],
			    "NameLast"=>$_POST["NameLast"],
			    "Street"=>$_POST["Street"],
			    "City"=>$_POST["City"],
			    "ZIP"=>$_POST["ZIP"],
			    "State"=>$_POST["State"],
			    "CountryID"=>$_POST["CountryID"],
			    "PhoneHome"=>$_POST["PhoneHome"],
			    "PhoneOffice"=>$_POST["PhoneOffice"],
			    "PhoneDay"=>$_POST["PhoneDay"],
			    "PhoneMobile"=>$_POST["PhoneMobile"],
			    "FAX"=>$_POST["FAX"],
			    "Website"=>$_POST["Website"],
			    "DateBorn"=>"{$_POST["DateBornYear"]}-{$_POST["DateBornMonth"]}-{$_POST["DateBornDay"]}",
			    "UserPicture"=>$_POST["UserPicture"],
				"RegistrationCode"=>GUID().GUID(),
				"UserIsRegistered"=>$_POST["UserIsRegistered"]
			),
			$Where
		);

		$MainContent.=CTL_Window(
				$Title="Operation successful",
				$Content="The user data has been stored successfully, please click <a href=\"".ApplicationURL($Script=$EntityLower."manage")."\">here</a> to continue."
			)."
		    <script language=\"JavaScript\">
		    <!--
		        window.location='".ApplicationURL($Script=$EntityLower."manage")."';
		    //-->
			</script>
		";
	}
?>