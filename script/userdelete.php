<?
    $Entity="User";
    $EntityAlias="U";
    $EntityLower=strtolower($Entity);
    $EntityCaption="User";
    $EntityCaptionLower=strtolower($EntityCaption);

	$MainContent.=CTL_Window(
		"Delete user",
		"
			<form action=\"".ApplicationURL($Script=$EntityLower."deleteaction", $Entity."ID=".$_REQUEST[$Entity."ID"]."&".$Entity."UUID=".$_REQUEST[$Entity."UUID"]."")."\" method=\"post\">
				Are you sure you want to remove the selected user?<br>
				<br>
				<div align=\"right\">
				    ".CTL_InputSubmit("", $Value="No")."
				    ".CTL_InputSubmit("DeleteConfirm", $Value="Yes")."
				</div>
			</form>
		",
		0,
		$Icon="question"
	);
?>
