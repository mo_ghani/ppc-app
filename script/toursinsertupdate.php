<?
    $Entity="Tours";
    $EntityAlias="T";
    $EntityLower=strtolower($Entity);
    $EntityCaption="Tours";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
    $FormTitle="Insert $EntityCaption";
    $ButtonCaption="Insert";
    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction");
    $Tours=array(
        "ToursName"=>"",
		"ToursPrice"=>"",
		"ToursDescription"=>"",
		"ToursPic"=>""
	);

	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"])){
	    $UpdateMode=true;
	    $FormTitle="Update $EntityCaption";
	    $ButtonCaption="Update";
	    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction", $Entity."ID={$_REQUEST[$Entity."ID"]}&".$Entity."UUID={$_REQUEST[$Entity."UUID"]}");

		if($UpdateMode&&!isset($_POST["".$Entity."Name"]))$Tours=SQL_Select($Entity="Tours", $Where="{$EntityAlias}.{$Entity}ID = {$_REQUEST[$Entity."ID"]} AND {$EntityAlias}.{$Entity}UUID = '{$_REQUEST[$Entity."UUID"]}'", $OrderBy="{$EntityAlias}.{$Entity}Name", $SingleRow=true);
	}

	$Input=array();
    $Input[]=array("VariableName"=>"ToursName", "DefaultValue"=>$Tours["ToursName"], "Caption"=>"Name", "ControlHTML"=>CTL_InputText("ToursName", $Tours["ToursName"], "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"ToursPrice", "DefaultValue"=>$Tours["ToursPrice"], "Caption"=>"Price", "ControlHTML"=>CTL_InputText("ToursPrice", $Tours["ToursPrice"], "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"ToursDescription", "DefaultValue"=>$Tours["ToursDescription"], "Caption"=>"Description", "ControlHTML"=>CTL_InputTextArea($Name="ToursDescription", $Value=$Tours["ToursDescription"], $Columns=89, $Rows=3), "Required"=>false);
    $Input[]=array("VariableName"=>"ToursPic", "DefaultValue"=>$Tours["ToursPic"], "Caption"=>"Image", "ControlHTML"=>CTL_ImageUpload($ControlName="ToursPic", $CurrentImage=$Tours["ToursPic"], $AllowDelete=$UpdateMode, $Class="FormTextInput", $ThumbnailHeight=100, $ThumbnailWidth=0, $Preview=$UpdateMode), "Required"=>false);

	$MainContent.=FormInsertUpdate(
		$EntityName=$EntityLower,
		$FormTitle,
		$Input,
		$ButtonCaption,
		$ActionURL
	);
?>