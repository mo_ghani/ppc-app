<?
    $Entity="User";
    $EntityAlias="U";
    $EntityLower=strtolower($Entity);
    $EntityCaption="User";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
    $FormTitle="Insert $EntityCaption";
    $ButtonCaption="Insert";
    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction");
    $User=array(
	        "UserName"=>"",
	        "UserPassword"=>"",
	        "UserEmail"=>"",
	        "UserDescription"=>"",
	        "UserTypeID"=>3,
	        "UserIsActive"=>1,
	        "NameFirst"=>"",
	        "NameMiddle"=>"",
	        "NameLast"=>"",
	        "Street"=>"",
	        "City"=>"",
	        "ZIP"=>"",
	        "State"=>"",
	        "CountryID"=>2,
	        "PhoneHome"=>"",
	        "PhoneOffice"=>"",
	        "PhoneDay"=>"",
	        "PhoneMobile"=>"",
	        "FAX"=>"",
	        "Website"=>"",
	        "DateBorn"=>"",
	        "UserPicture"=>"",
			"UserIsRegistered"=>0,
	);

	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"])){
	    $UpdateMode=true;
	    $FormTitle="Update $EntityCaption";
	    $ButtonCaption="Update";
	    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction", $Entity."ID={$_REQUEST[$Entity."ID"]}&".$Entity."UUID={$_REQUEST[$Entity."UUID"]}");

		if($UpdateMode&&!isset($_POST["".$Entity."Name"]))$User=SQL_Select($Entity="User", $Where="{$EntityAlias}.{$Entity}ID = {$_REQUEST[$Entity."ID"]} AND {$EntityAlias}.{$Entity}UUID = '{$_REQUEST[$Entity."UUID"]}'", $OrderBy="{$EntityAlias}.{$Entity}Name", $SingleRow=true);
	}

	$_POST["UserPassword"]=$_POST["UserPasswordConfirm"]="";

	$Input=array();
    $Input[]=array("VariableName"=>"UserName", "DefaultValue"=>$User["UserName"], "Caption"=>"Username", "ControlHTML"=>CTL_InputText("UserName", $User["UserName"], "", 61, $Class="FormInputText", $Style="", $ReadOnly=false, $Debug=false), "Required"=>true);
    $Input[]=array("VariableName"=>"UserEmail", "DefaultValue"=>$User["UserEmail"], "Caption"=>"Email", "ControlHTML"=>CTL_InputText("UserEmail", $User["UserEmail"], "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"UserDescription", "DefaultValue"=>$User["UserDescription"], "Caption"=>"Description", "ControlHTML"=>CTL_InputTextArea("UserDescription", $User["UserDescription"], $Columns=60, $Rows=5), "Required"=>false);
    $Input[]=array("VariableName"=>"UserPassword", "DefaultValue"=>"", "Caption"=>"Password", "ControlHTML"=>CTL_InputPassword("UserPassword", "", "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"UserPasswordConfirm", "DefaultValue"=>"", "Caption"=>"Confirm password", "ControlHTML"=>CTL_InputPassword("UserPasswordConfirm", "", "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"UserTypeID", "DefaultValue"=>$User["UserTypeID"], "Caption"=>"Type", "ControlHTML"=>CCTL_UserTypeLookup($Name="UserTypeID", $ValueSelected=$User["UserTypeID"], $Where="UT.UserTypeID <> {$Application["UserTypeIDGuest"]}"), "Required"=>false);
    $Input[]=array("VariableName"=>"NameFirst", "DefaultValue"=>$User["NameFirst"], "Caption"=>"First name", "ControlHTML"=>CTL_InputText("NameFirst", $User["NameFirst"], "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"NameMiddle", "DefaultValue"=>$User["NameMiddle"], "Caption"=>"Middle name", "ControlHTML"=>CTL_InputText("NameMiddle", $User["NameMiddle"], "", 61), "Required"=>false);
    $Input[]=array("VariableName"=>"NameLast", "DefaultValue"=>$User["NameLast"], "Caption"=>"Last name", "ControlHTML"=>CTL_InputText("NameLast", $User["NameLast"], "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"DateBorn", "DefaultValue"=>$User["DateBorn"], "Caption"=>"Date of Birth", "ControlHTML"=>CTL_DateSelector($DateSelectorName="DateBorn", $SelectedDate=$User["DateBorn"]), "Required"=>false);
    $Input[]=array("VariableName"=>"UserPicture", "DefaultValue"=>$User["UserPicture"], "Caption"=>"Image", "ControlHTML"=>CTL_ImageUpload($ControlName="UserPicture", $CurrentImage=$User["UserPicture"], $AllowDelete=$UpdateMode, $Class="FormTextInput", $ThumbnailHeight=100, $ThumbnailWidth=0, $Preview=$UpdateMode)."<br><br>", "Required"=>false);
    $Input[]=array("VariableName"=>"Street", "DefaultValue"=>$User["Street"], "Caption"=>"Street", "ControlHTML"=>CTL_InputText("Street", $User["Street"], "", 61), "Required"=>false);
    $Input[]=array("VariableName"=>"City", "DefaultValue"=>$User["City"], "Caption"=>"City", "ControlHTML"=>CTL_InputText("City", $User["City"], "", 61), "Required"=>false);
    $Input[]=array("VariableName"=>"ZIP", "DefaultValue"=>$User["ZIP"], "Caption"=>"ZIP", "ControlHTML"=>CTL_InputText("ZIP", $User["ZIP"], "", 61), "Required"=>false);
    $Input[]=array("VariableName"=>"State", "DefaultValue"=>$User["State"], "Caption"=>"State", "ControlHTML"=>CTL_InputText("State", $User["State"], "", 61), "Required"=>false);
    $Input[]=array("VariableName"=>"CountryID", "DefaultValue"=>$User["CountryID"], "Caption"=>"Country", "ControlHTML"=>CCTL_CountryLookup($Name="CountryID", $ValueSelected=$User["CountryID"]), "Required"=>false);
    $Input[]=array("VariableName"=>"PhoneHome", "DefaultValue"=>$User["PhoneHome"], "Caption"=>"Home phone", "ControlHTML"=>CTL_InputText("PhoneHome", $User["PhoneHome"], "", 61), "Required"=>false);
    $Input[]=array("VariableName"=>"PhoneOffice", "DefaultValue"=>$User["PhoneOffice"], "Caption"=>"Office phone", "ControlHTML"=>CTL_InputText("PhoneOffice", $User["PhoneOffice"], "", 61), "Required"=>false);
    $Input[]=array("VariableName"=>"PhoneDay", "DefaultValue"=>$User["PhoneDay"], "Caption"=>"Day phone", "ControlHTML"=>CTL_InputText("PhoneDay", $User["PhoneDay"], "", 61), "Required"=>false);
    $Input[]=array("VariableName"=>"PhoneMobile", "DefaultValue"=>$User["PhoneMobile"], "Caption"=>"Mobile phone", "ControlHTML"=>CTL_InputText("PhoneMobile", $User["PhoneMobile"], "", 61), "Required"=>false);
    $Input[]=array("VariableName"=>"FAX", "DefaultValue"=>$User["FAX"], "Caption"=>"FAX", "ControlHTML"=>CTL_InputText("FAX", $User["FAX"], "", 61), "Required"=>false);
    $Input[]=array("VariableName"=>"Website", "DefaultValue"=>$User["Website"], "Caption"=>"Website", "ControlHTML"=>CTL_InputText("Website", $User["Website"], "", 61)."<br><br>", "Required"=>false);

    $Input[]=array("VariableName"=>"SectionTitleRow", "DefaultValue"=>"", "Caption"=>"Administrator", "ControlHTML"=>"", "Required"=>False);
    $Input[]=array("VariableName"=>"UserIsActive", "DefaultValue"=>$User["UserIsActive"], "Caption"=>"Active?", "ControlHTML"=>CTL_InputRadioSet($VariableName="UserIsActive", $Captions=array("Yes", "No"), $Values=array(1, 0), $CurrentValue=$User["UserIsActive"]), "Required"=>false);
    $Input[]=array("VariableName"=>"UserIsRegistered", "DefaultValue"=>$User["UserIsRegistered"], "Caption"=>"Is Registered?", "ControlHTML"=>CTL_InputRadioSet($VariableName="UserIsRegistered", $Captions=array("Yes", "No"), $Values=array(1, 0), $CurrentValue=$User["UserIsRegistered"]), "Required"=>false);

	$MainContent.=FormInsertUpdate(
		$EntityName=$EntityLower,
		$FormTitle,
		$Input,
		$ButtonCaption,
		$ActionURL
	);
?>