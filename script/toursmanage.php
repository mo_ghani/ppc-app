<?
    $Entity="Tours";
    $EntityAlias="T";
    $EntityLower=strtolower($Entity);
    $EntityCaption="Tours";
    $EntityCaptionLower=strtolower($EntityCaption);

	SetFormvariable("RecordShowFrom", 1);
    SetFormvariable("RecordShowUpTo", $Application["DatagridRowsDefault"]);
    SetFormvariable("SortBy", "ToursName");
    SetFormvariable("SortType", "ASC");

	if(isset($_POST["ActionNew{$Entity}"])){
	include "./script/".$EntityLower."insertupdate.php";
	}else{

    $ControlHTML="
		".CTL_InputSubmit($Name="ActionNew{$Entity}", $Value="New {$EntityCaption}")."
	";

    $SearchHTML="
		Search ".CTL_InputText($Name="FreeText")."
	";

    $Where=" 1=1 ";
	if($_POST["FreeText"]!="")$Where.=" AND ({$EntityAlias}.{$Entity}Name LIKE '%{$_POST["FreeText"]}%' OR CountryName LIKE '%{$_POST["FreeText"]}%')";

	$MainContent.= CTL_Datagrid(
		$Entity,
		$ColumnName=array("", "ToursPic", "{$Entity}Name", "{$Entity}Price"),
		$ColumnTitle=array("", "", "Name", "Price"),
		$ColumnAlign=array("", "left", "left", "left"),
		$ColumnType=array("", "imagelink", "text", "text"),
		$Rows=SQL_Select($Entity="Tours", $Where, $OrderBy="{$_REQUEST["SortBy"]} {$_REQUEST["SortType"]}", $SingleRow=false, $RecordShowFrom=$_POST["RecordShowFrom"], $RecordShowUpTo=$_POST["RecordShowUpTo"], $Debug=false),
		$SearchHTML,
		$ControlHTML,
		$AdditionalLinks=array(),
		$AdditionalActionParameter="",
		$ActionLinks=true,
		$SearchPanel=true,
		$ControlPanel=true,
		$CheckBox=false,
		$EntityAlias="".$EntityCaption."",
		$SortLinkExtraParameter="",
		$ControlPanelFormActionExtraParameter=""
	);
	}
?>