<?php
include("db.php");

connect_my_database();

ini_set('date.timezone', 'Asia/Dhaka');
$today=date('Y-m-d');

function logger($msg){


	if ($fp = fopen('callogfile.log', 'a')) {    // Open and write to mailcetner log file


        $message=date('d-m-Y,h.i.s A').": ".$msg."\n";


        fwrite($fp, $message);


        fclose($fp);


 	}


}
logger("-------------------Commission Calculation Start----------------------");

$qryC = "update tblcommissionstatus set CommissionDate='$today',Status='R'";

if (!$rsC = mysql_query($qryC)){
	$em = "Error in query: ".mysql_error();
	logger($em);
	die();
}
//print "1";
mysql_query("BEGIN");
$str = "select a.UserID,NewLeftBV,NewRightBV,NewLeftInv,NewRightInv,BVMatchingLimit,IncomeLimit,TotalAmount,AmountPayable,Chrgd,Balance,c.BV,BVmatchingRate,InvestMatchingRate,ServiceCharge from tblmemberstatus a,tblpayment b,tblrank c where a.UserID=b.UserID and c.RankID=a.RankId and status='U' and NewLeftBV>=30 and NewRightBV>=30";
if (!$rs = mysql_query($str)){

	$errmsg="Error in query: ".mysql_error();
	logger($errmsg);
	die();
}else{
	//echo mysql_num_rows($rs)."<br>";
	while($row = mysql_fetch_array($rs)){
		$BVcommissionable = false;
		$invCommissionable = false;
		$flashedInv = false;
		$flashedBV = false;
		$userID =  $row['UserID'];
		$newLeftBV = $row['NewLeftBV'];
		$newRightBV = $row['NewRightBV'];
		$newLeftInv = $row['NewLeftInv'];
		$newRightInv = $row['NewRightInv'];
		$bvMatchingLimit = $row['BVMatchingLimit'];
		$investIncomeLimit = $row['IncomeLimit'];
		$totalAmount = $row['TotalAmount'];
		$AmountPayable = $row['AmountPayable'];
		$totalChrgd = $row['Chrgd'];
		$presentBalance = $row['Balance'];
		$BV = $row['BV'];
		$BVmatchingRate = $row['BVmatchingRate'];
		$InvestMatchingRate = $row['InvestMatchingRate'];
		$ServiceCharge = $row['ServiceCharge'];

		$lastDay = date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d"), date("Y")));

		$upd = "";
		//$bvMatchingLimit = $row['BVMatchingLimit'];
        if($newLeftBV >=30 AND $newRightBV>=30){
        	//echo "BV";

			$BVcommissionable = true;

			if($newLeftBV >= $newRightBV){

        		$large = "left";

        		$matchedBV = $newRightBV;

        		$extLeftBV = $newLeftBV - $newRightBV;

        		$extRightBV = $newRightBV - $newRightBV;

        	}else{

        		$large = "right";


        		$matchedBV = $newLeftBV;


        		$extLeftBV = $newLeftBV - $newLeftBV;


        		$extRightBV = $newRightBV - $newLeftBV;


        	}
			
			$BVBeforeFlash = $matchedBV;
			
			if($matchedBV>=30 AND $matchedBV<60){
				$flashedBV = true;
            	$matchedBV = 30;
			}elseif($matchedBV>=60 AND $matchedBV<120){
				$flashedBV = true;
            	$matchedBV = 60;
            }elseif($matchedBV>=120 AND $matchedBV<220){
            	$flashedBV = true;
				$matchedBV = 120;
            }elseif($matchedBV>=220 AND $matchedBV<520){
            	$flashedBV = true;
				$matchedBV = 220;
            }elseif($matchedBV>=520 AND $matchedBV<1120){
            	$flashedBV = true;
				$matchedBV = 520;
            }elseif($matchedBV>=1120 AND $matchedBV<3020){
            	$flashedBV = true;
				$matchedBV = 1120;
            }elseif($matchedBV>=3020 AND $matchedBV<8020){
            	$flashedBV = true;
				$matchedBV = 3020;
            }elseif($matchedBV>=8020 AND $matchedBV<18020){
            	$flashedBV = true;
				$matchedBV = 8020;
            }elseif($matchedBV>=18020 AND $matchedBV<30030){
            	$flashedBV = true;
				$matchedBV = 18020;
            }elseif($matchedBV>=30030){
            	$flashedBV = true;
				$matchedBV = 30030;
            }
			/*if($matchedBV > $bvMatchingLimit){
				$flashedBV = true;


				$BVBeforeFlash = $matchedBV;


				$matchedBV = $bvMatchingLimit;

			}*/

			$bvCommission = ($matchedBV * $BVmatchingRate)/100;

			$bvChrgToBeDeduct = ($bvCommission*$ServiceCharge)/100;

			$bvCommissionGet = $bvCommission - $bvChrgToBeDeduct;

        }
/*

		if($newLeftInv>=200 AND $newRightInv>=200){


			//echo "Inv";


			$invCommissionable = true;


			if($newLeftInv >= $newRightInv){


        		$large = "left";


        		$commonInv = $newRightInv;


        		//$extLeftInv = $newLeftInv - $newRightInv;


        		//$extRightInv = $newRightBV - $newRightBV;


        	}else{


        		$large = "right";


        		$commonInv = $newLeftInv;


        		//$extLeftInv = $newLeftInv - $newLeftInv;


        		//$extRightInv = $newRightInv - $newLeftInv;


			}


            $cfLeftInv = $newLeftInv - $commonInv;


            $cfRightInv = $newRightInv - $commonInv;


			$actualCommonInv = $commonInv;


			if($commonInv>=200 AND $commonInv<600){


            	$commonInv = 200;


			}elseif($commonInv>=600 AND $commonInv<1000){


            	$commonInv = 600;


            }elseif($commonInv>=1000 AND $commonInv<2000){


            	$commonInv = 1000;


			}elseif($commonInv>=2000 AND $commonInv<3000){


            	$commonInv = 2000;


			}elseif($commonInv>=3000 AND $commonInv<5000){


            	$commonInv = 3000;


			}elseif($commonInv>=5000 AND $commonInv<10000){


            	$commonInv = 5000;


			}elseif($commonInv>=10000 AND $commonInv<15000){


				$commonInv = 10000;


			}elseif($commonInv>=15000 AND $commonInv<20000){


				$commonInv = 15000;


			}elseif($commonInv>=20000 AND $commonInv<30000){


				$commonInv = 20000;


			}elseif($commonInv>=30000 AND $commonInv<50000){


				$commonInv = 30000;


			}elseif($commonInv>=50000 AND $commonInv<100000){


				$commonInv = 50000;


			}elseif($commonInv>=100000 AND $commonInv<200000){


				$commonInv = 100000;


			}elseif($commonInv>=200000){


				$commonInv = 200000;


			}


            if ($commonInv > $investIncomeLimit){


				//$flashedInv = true;


				//$invBeforeFlash = $commonInv;


				$commonInv = $investIncomeLimit;


			}


			$invCommission = ($commonInv * $InvestMatchingRate)/100;


			$invChrgToBeDeduct = ($invCommission*$ServiceCharge)/100;


			$invCommissionGet = $invCommission - $invChrgToBeDeduct;


		}
		*/
		/*elseif($newLeftInv < 500){


			$upd .= "LeftSideInvRem=$newLeftInv,";


		}elseif($newRightInv < 500){


			$upd .= "RightSideInvRem=$newRightInv,";


		}*/


		if($BVcommissionable == true){

			$updateQry = "update tblmemberstatus set ";

			$todayPay = $invCommission + $bvCommission;

			$todaySerChrg = $invChrgToBeDeduct + $bvChrgToBeDeduct;
			$todayPaidAmt = $invCommissionGet + $bvCommissionGet;

			$todayBalance = $todayPaidAmt;

			if($BVcommissionable==true){


				if($flashedBV == true){
					$updateQry .= "LeftSideBV=LeftSideBV+$BVBeforeFlash,RightSideBV=RightSideBV+$BVBeforeFlash,LeftSideBVrem=$extLeftBV,RightSideBVrem=$extRightBV,newLeftBV=$extLeftBV, newRightBV=$extRightBV,";

				}else{

					$updateQry .= "LeftSideBV=LeftSideBV+$matchedBV,RightSideBV=RightSideBV+$matchedBV,LeftSideBVrem=$extLeftBV,RightSideBVrem=$extRightBV,newLeftBV=$extLeftBV, newRightBV=$extRightBV,";


				}


				//$updateQry .= "newLeftBV=$extLeftBV, newRightBV=$extRightBV,";


				$insertTrDetMatch = "insert into tbltransactiondetail(UserIDInserted,TransactionDetailDate,Amount,ServiceCharge,AmountPayableTr,Balance,";


				$insertTrDetMatch .= "TransactionDetailType,Description,ToFrom,ToFromUserName,DateInserted)";


				$insertTrDetMatch .= " values($userID,'$lastDay',$bvCommission,$bvChrgToBeDeduct,$bvCommissionGet,$presentBalance+$bvCommissionGet,'Cr','Affiliate Matching Bonus','From Matching','Self',now())";


				//echo "<br>$insertTrDetMatch";


				if (!mysql_query($insertTrDetMatch)){

					$errmsg="Insert Detailed Transaction-1 Failed: ".mysql_error();
					logger($errmsg); die();
				}

			}else{

				if($newLeftBV > 0 AND $newRightBV==0){

				//echo $newLeftBV;

					$updateQry .= "LeftSideBVrem=$newLeftBV,";

				}elseif($newRightBV > 0 AND $newLeftBV==0){


					$updateQry .= "RightSideBVrem=$newRightBV,";


					//echo "2";


				}


				$newLeftBV = 0;


				$newRightBV = 0;


				$extLeftBV = 0;


				$extRightBV = 0;


				$matchedBV = 0;


				$bvCommissionGet = 0;


			}

			// commented for new ustourism
			/*
			if($invCommissionable == true){


				$updateQry .= "LeftSideInv=LeftSideInv+$actualCommonInv,RightSideInv=RightSideInv+$actualCommonInv,LeftSideInvRem=$cfLeftInv,RightSideInvRem=$cfRightInv,NewLeftInv=$cfLeftInv,NewRightInv=$cfRightInv,";


				$insertTrDetInv = "insert into tbltransactiondetail(UserIDInserted,TransactionDetailDate,Amount,ServiceCharge,AmountPayableTr,Balance,";


				$insertTrDetInv .= "TransactionDetailType,Description,ToFrom,ToFromUserName)";


				$insertTrDetInv .= " values($userID,'$lastDay',$invCommission,$invChrgToBeDeduct,$invCommissionGet,$presentBalance+$todayBalance,'Cr','Investment Matching Bonus','From Matching','Self')";


				//echo "<br>$insertTrDetInv";


				if (!mysql_query($insertTrDetInv)){





					$errmsg="Insert Detailed Transaction-2 Failed: ".mysql_error();





					logger($errmsg); die();





				}


			}else{


				if($newLeftInv < 500 or $newRightInv < 500){


					$updateQry .= "LeftSideInvRem=$newLeftInv,RightSideInvRem=$newRightInv,";


					//echo "3";


				}


				$newLeftInv = 0;


				$newRightInv = 0;


				$cfLeftInv = 0;


				$cfRightInv = 0;


				$commonInv = 0;


				$invCommissionGet = 0;


			}*/


			$updateQry .= "status='C' where UserID=$userID";

			if (!mysql_query($updateQry)){

				$errmsg="Update Failed: ".mysql_error();
				logger($errmsg); die();
			}
			// commented for new ustourism
			/*
			$insertMatchDetail = "insert into tblmatchingdetail(UserID,LeftBV,RightBV,CFLeftBV,CFRightBV,MatchingBV,MatchingBonusBV,LeftInv,RightInv,";
			$insertMatchDetail .= "CFLeftInv,CFRightInv,MatchingInv,MatchingBonusInv,date) values($userID,$newLeftBV,$newRightBV,$extLeftBV,$extRightBV,";
			$insertMatchDetail .= "$matchedBV,$bvCommissionGet,$newLeftInv,$newRightInv,$cfLeftInv,$cfRightInv,$commonInv,$invCommissionGet,'$lastDay')";
*/
			$insertMatchDetail = "insert into tblmatchingdetail(UserID,LeftBV,RightBV,CFLeftBV,CFRightBV,MatchingBV,MatchingBonusBV,date)";
			$insertMatchDetail .= " values($userID, $newLeftBV,$newRightBV,$extLeftBV,$extRightBV,$matchedBV,$bvCommissionGet,'$lastDay')";
			

			//echo "<br>$insertMatchDetail";


			if (!mysql_query($insertMatchDetail)){
				$errmsg="Insert Detailed Matching Failed: ".mysql_error();
				logger($errmsg); die();
			}

			$updPayment = "update tblpayment set TotalAmount=TotalAmount+$todayPay,Balance=Balance+$todayBalance,Chrgd=Chrgd+$todaySerChrg,AmountPayable=AmountPayable+$todayPaidAmt where UserID=$userID";
			//echo "<br>$updPayment";
			if (!mysql_query($updPayment)){
				$errmsg="Update Payment Failed: ".mysql_error();
				logger($errmsg); die();
			}
		}else{
			if($newLeftBV > 0 AND $newRightBV==0){
				//echo $newLeftBV;
				$upd = "LeftSideBVrem=$newLeftBV,";
			}elseif($newRightBV > 0 AND $newLeftBV==0){
				$upd = "RightSideBVrem=$newRightBV,";
				//echo "2";
			}
			// commented for new ustourisms
			/*if($newLeftInv < 500 or $newRightInv < 500){
				$upd .= "LeftSideInvRem=$newLeftInv,RightSideInvRem=$newRightInv,";
				//echo "3";
			}*/
			$updateQry = "update tblmemberstatus set $upd status='C' where UserID=$userID";
			if(!mysql_query($updateQry)){
				$em = "Update Failed: ".mysql_error();
				logger($em); die();
			}
		}
	}
}
$qryC = "update tblcommissionstatus set Status='S'";
if (!$rsC = mysql_query($qryC)){
	$em = "Error in query: ".mysql_error();
	logger($errmsg);
	die();
}
//include("investmentMaturityCalc.php");
//investmentmaturedInvestment();
logger("-------------------Commission Calculation End------------------------");
logger("-------------------Maturity Calculation Start------------------------");
include("investmentMaturityCalc.php");
investmentmaturedInvestment();
logger("-------------------Maturity Calculation End------------------------");
mysql_query("COMMIT");
?>