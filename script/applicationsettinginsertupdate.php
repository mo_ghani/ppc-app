<?
   $Entity="ApplicationSetting";
    $EntityAlias="APS";
    $EntityLower=strtolower($Entity);
    $EntityCaption="Application Setting";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
    $FormTitle="Insert $EntityCaption";
    $ButtonCaption="Insert";
    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction");
    $ApplicationSetting=array(
        "ApplicationSettingName"=>"",
        "ApplicationSettingValue"=>"",
        "ApplicationSettingDescription"=>"",
        "ApplicationSettingInputTypeName"=>"",
        "ApplicationSettingIsHidden"=>0,
        "ApplicationSettingIsLocked"=>0,
        "ApplicationSettingIsActive"=>1
	);

	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"])){
	    $UpdateMode=true;
	    $FormTitle="Update $EntityCaption";
	    $ButtonCaption="Update";
	    $ActionURL=ApplicationURL($Script=$EntityLower."insertupdateaction", $Entity."ID={$_REQUEST[$Entity."ID"]}&".$Entity."UUID={$_REQUEST[$Entity."UUID"]}");

		if($UpdateMode&&!isset($_POST["".$Entity."Name"]))$ApplicationSetting=SQL_Select($Entity="ApplicationSetting", $Where="{$EntityAlias}.{$Entity}ID = {$_REQUEST[$Entity."ID"]} AND {$EntityAlias}.{$Entity}UUID = '{$_REQUEST[$Entity."UUID"]}'", $OrderBy="{$EntityAlias}.{$Entity}Name", $SingleRow=true);
	}

	$Input=array();
    $Input[]=array("VariableName"=>"ApplicationSettingName", "DefaultValue"=>$ApplicationSetting["ApplicationSettingName"], "Caption"=>"Name", "ControlHTML"=>CTL_InputText("ApplicationSettingName", $ApplicationSetting["ApplicationSettingName"], "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"ApplicationSettingValue", "DefaultValue"=>$ApplicationSetting["ApplicationSettingValue"], "Caption"=>"Value", "ControlHTML"=>CTL_InputText("ApplicationSettingValue", $ApplicationSetting["ApplicationSettingValue"], "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"ApplicationSettingDescription", "DefaultValue"=>$ApplicationSetting["ApplicationSettingDescription"], "Caption"=>"Description", "ControlHTML"=>CTL_InputTextArea($Name="ApplicationSettingDescription", $Value=$ApplicationSetting["ApplicationSettingDescription"], $Columns=61, $Rows=3), "Required"=>false);
    $Input[]=array("VariableName"=>"ApplicationSettingInputTypeName", "DefaultValue"=>$ApplicationSetting["ApplicationSettingInputTypeName"], "Caption"=>"Type", "ControlHTML"=>CTL_InputText("ApplicationSettingInputTypeName", $ApplicationSetting["ApplicationSettingInputTypeName"], "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"ApplicationSettingIsHidden", "DefaultValue"=>$ApplicationSetting["ApplicationSettingIsHidden"], "Caption"=>"Hidden?", "ControlHTML"=>CTL_InputRadioSet($VariableName="ApplicationSettingIsHidden", $Captions=array("Yes", "No"), $Values=array(1, 0), $CurrentValue=$ApplicationSetting["ApplicationSettingIsHidden"]), "Required"=>false);
    $Input[]=array("VariableName"=>"ApplicationSettingIsLocked", "DefaultValue"=>$ApplicationSetting["ApplicationSettingIsLocked"], "Caption"=>"Locked?", "ControlHTML"=>CTL_InputRadioSet($VariableName="ApplicationSettingIsLocked", $Captions=array("Yes", "No"), $Values=array(1, 0), $CurrentValue=$ApplicationSetting["ApplicationSettingIsLocked"]), "Required"=>false);
    $Input[]=array("VariableName"=>"ApplicationSettingIsActive", "DefaultValue"=>$ApplicationSetting["ApplicationSettingIsActive"], "Caption"=>"Active?", "ControlHTML"=>CTL_InputRadioSet($VariableName="ApplicationSettingIsActive", $Captions=array("Yes", "No"), $Values=array(1, 0), $CurrentValue=$ApplicationSetting["ApplicationSettingIsActive"]), "Required"=>false);

	$MainContent.=FormInsertUpdate(
		$EntityName=$EntityLower,
		$FormTitle,
		$Input,
		$ButtonCaption,
		$ActionURL
	);





/*

    $Entity="ApplicationSetting";
    $EntityLower=strtolower($Entity);
    $UpdateMode=false;
	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"]))$UpdateMode=true;
	if($UpdateMode&&!isset($_POST["ApplicationSettingName"])){
	    $ApplicationSetting=SQL_ApplicationSettingSelect($Where="APS.".$Entity."ID = {$_REQUEST["ApplicationSettingID"]} AND APS.".$Entity."UUID = '{$_REQUEST["ApplicationSettingUUID"]}'", $OrderBy="APS.".$Entity."Name", $SingleRow=true);
	}else{
	    $ApplicationSetting=array(
	        "ApplicationSettingTypeID"=>0,
	        "ApplicationSettingName"=>"",
	        "ApplicationSettingValue"=>""
		);
	}
	if($UpdateMode){
	    $FormTitle="Update ApplicationSetting";
	    $ButtonCaption="Update";
	    $ActionURL=ApplicationURL($_REQUEST["Area"], $EntityLower."insertupdateaction", $Entity."ID={$_REQUEST[$Entity."ID"]}&".$Entity."UUID={$_REQUEST[$Entity."UUID"]}");
	}else{
	    $FormTitle="Create ApplicationSetting";
	    $ButtonCaption="Insert";
	    $ActionURL=ApplicationURL($_REQUEST["Area"], $EntityLower."insertupdateaction");
	}

	$Input=array();
    $Input[]=array("VariableName"=>"ApplicationSettingTypeID", "DefaultValue"=>$ApplicationSetting["ApplicationSettingTypeID"], "Caption"=>"Type", "ControlHTML"=>CCTL_ApplicationSettingTypeLookup($Name="ApplicationSettingTypeID", $ValueSelected=$ApplicationSetting["ApplicationSettingTypeID"], $Where="", $PrependBlankOption=false), "Required"=>false);
    $Input[]=array("VariableName"=>"ApplicationSettingName", "DefaultValue"=>$ApplicationSetting["ApplicationSettingName"], "Caption"=>"Setting Name", "ControlHTML"=>CTL_InputText("ApplicationSettingName", $ApplicationSetting["ApplicationSettingName"], "", 61), "Required"=>true);
    $Input[]=array("VariableName"=>"ApplicationSettingValue", "DefaultValue"=>$ApplicationSetting["ApplicationSettingValue"], "Caption"=>"Value", "ControlHTML"=>CTL_InputText("ApplicationSettingValue", $ApplicationSetting["ApplicationSettingValue"], "", 61), "Required"=>false);

	$MainContent.=FormInsertUpdate(
		$EntityName=$EntityLower,
		$FormTitle,
		$Input,
		$ButtonCaption,
		$ActionURL
	);
*/
?>