<?
    $Entity="AdvertPanel";
    $EntityAlias="AP";
    $EntityLower=strtolower($Entity);
    $EntityCaption="Advert Panel";
    $EntityCaptionLower=strtolower($EntityCaption);

    $UpdateMode=false;
	if(isset($_REQUEST[$Entity."ID"])&&isset($_REQUEST[$Entity."UUID"]))$UpdateMode=true;

    $ErrorUserInput["_Error"]=false;
    CheckRequiredFormVariables(
		$Variable=array(
			array("Name"=>"AdvertPanelName", "Message"=>"Please provide with the user type name.")
		)
	);

    if($ErrorUserInput["_Error"]){
        include "./script/".$EntityLower."insertupdate.php";
	}else{
	    $Where="";
	    if($UpdateMode)$Where="{$Entity}ID = {$_REQUEST["AdvertPanelID"]} AND {$Entity}UUID = '{$_REQUEST["AdvertPanelUUID"]}'";

		//$_POST["AdvertPanelPicture"]=ProcessUpload("AdvertPanelPicture", $Application["UploadPath"]);

	    $AdvertPanel=SQL_InsertUpdate(
	        $Entity,
	        $EntityAlias,
			$AdvertPanelData=array(
			    "AdvertPanelName"=>$_POST["AdvertPanelName"],
			    "AdvertPanelIdentifire"=>$_POST["AdvertPanelIdentifire"],
			    "AdvertWidth"=>$_POST["AdvertWidth"],
			    "AdvertHeight"=>$_POST["AdvertHeight"],
			    "AdvertPanelMaxNumber"=>$_POST["AdvertPanelMaxNumber"],
			    "AdvertPanelIsVertical"=>$_POST["AdvertPanelIsVertical"],
			    "AdvertPanelIsActive"=>$_POST["AdvertPanelIsActive"]
		),
			$Where
		);

	    $MainContent.="
	        ".CTL_Window($Title="Advert Panel management", "The operation complete successfully and<br>
			<br>
			the $EntityCaptionLower information has been stored.<br>
			<br>
			Please click <a href=\"".ApplicationURL($Script=$EntityLower."manage")."\">here</a> to proceed.")."
	        <script language=\"JavaScript\">
	        <!--
	            window.location='".ApplicationURL($Script=$EntityLower."manage")."';
	        -->
	        </script>
		";
	}
?>