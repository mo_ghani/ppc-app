<?
	if($_POST["UserName"]=="" || $_POST["InquiryFor"]=="" || $_POST["TxtInquiry"]=="" || $_POST["TxtEmail"]==""){
		$invalid="<span style=\"color:#FF0000; font-size:12px; font-weight:bold;\">You must fill up Name, Subject, Message, Email</span>";
		include ("contactus.php");
	}else{
		SendMail(
			$ToEmail=$Application["EmailContact"],
			$Subject="Inquiry from Client",
			$Body="
				The following user has contact with the Administration:<br>
				<br>
					<table width=\"100%\" border=\"0\" style=\"font-family:arial; font-size:12px;\">
						<tr>
							<td width=\"150px;\">Name:</td>
							<td align=\"left\">".$_POST["UserName"]." </td>
						</tr>
						<tr>
							<td width=\"150px;\">Country:</td>
							<td align=\"left\">".$_POST["Country"]." </td>
						</tr>
						<tr>
							<td>Inquiry For:</td>
							<td align=\"left\">
								".$_POST["InquiryFor"]."
							</td>
						</tr>
						<tr>
							<td>Inquiry:</td>
							<td align=\"left\">".$_POST["TxtInquiry"]."</td>
						</tr>
						<tr>
							<td colspan=2 style=\"font-family:MyriadBold; font-size:14px; color:#cc0000; padding-left:50px;\" align=left>Contact Details</td>
						</tr>
						<tr>
							<td>Company:</td>
							<td align=\"left\">".$_POST["CompanyName"]."</td>
						</tr>
						<tr>
							<td>Email:</td>
							<td align=\"left\">".$_POST["TxtEmail"]."</td>
						</tr>
						<tr>
							<td>Telephone:</td>
							<td align=\"left\">".$_POST["TxtPhoneCountry"]."-".$_POST["TxtPhoneArea"]."-".$_POST["TxtPhoneNumber"]."</td>
						</tr>
					</table>
			",
			$FromName=$Application["Title"],
			$FromEmail = $Application["EmailContact"],
			$ReplyToName=$Application["Title"],
			$ReplyToEmail=$Application["EmailContact"],
			$ExtraHeaderParameters=""
		);
	$MainContent.="
		<table width=\"850\" border=\"0\" align=\"center\" cellpadding=\"5\">
			<tr>
				<td>
					<br><br>
					<span style=\"color:#ED4F44; font-size:17px; font-weight:bold;\">
						Email Send
					</span>
					<br><br>
				</td>
			</tr>
			<tr>
				<td height=\"100%\" bgcolor=\"#F5F5F5\" style=\"font-size:12px;\" valign=\"top\"><br>
					Dear {$_POST["UserName"]},<br>
					<br>
					Thank you for Contacting thirdrex.com.<br>
					We will review your query and contact with you very soon.<br>
					<br>
					{$Application["Title"]}<br>
				</td>
			</tr>
			<tr>
				<td height=\"100\"></td>
			</tr>
		</table>
	";
	}	
?>