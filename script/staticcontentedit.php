<?
	/*
		Script:		
		Author:		Shahriar Kabir (SKJoy2001@Yahoo.Com)
		Date:		
		Purpose:	
		Note:		
	*/

	$UpdateMode=false;
	$ActionURL=ApplicationURL($Script="staticcontenteditaction&StaticContentName={$_REQUEST["StaticContentName"]}");
	$ButtonCaption="Insert";

    $StaticContent=SQL_Select($Entity="StaticContent", $Where="SC.StaticContentName = '{$_REQUEST["StaticContentName"]}' AND L.LanguageCode = '{$_REQUEST["LanguageCode"]}'", $OrderBy="SC.StaticContentName", $SingleRow=true, $RecordShowFrom=0, $RecordShowUpTo=0, $Debug=false);
	if(count($StaticContent)>1)$UpdateMode=true;

	if($UpdateMode){
		$ButtonCaption="Update";
	}else{
	    $StaticContent=array(
	        "StaticContent"=>""
		);
	}

	$MainContent.="
	    <a href=\"".ApplicationURL($Script="imagestorebrowser")."\" target=\"_blank\">Image Browser</a><hr noshade>
	".FormInsertUpdate(
		$EntityName="StaticContent",
		$FormTitle="Update Content",
		$Input=array(
		    array("VariableName"=>"StaticContent", "Caption"=>"", "DefaultValue"=>$StaticContent["StaticContent"], "Required"=>false, "ControlHTML"=>CTL_InputTextArea($Name="StaticContent", $Value=$StaticContent["StaticContent"], $Columns=86, $Rows=25))
		),
		$ButtonCaption,
		$ActionURL
	);
?>