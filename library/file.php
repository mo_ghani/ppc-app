<?

	//Save something in a disk file
    function file_put_contents2($FileName, $Content=""){
		//DebugFunctionTrace($FunctionName="file_put_contents2", $Parameter=array("FileName"=>$FileName, "Content"=>$Content), $UseURLDebugFlag=true);

        $File=fopen($FileName, "w");
        fwrite($File, $Content);
        fclose($File);
    }

	//Move the file from the temporary location of the PHP's upload path & rename the file accordingly. Returns the new filename on a
	//successful operation. Designed for application's internal purpose
    function FileUpload($RemoteFile, $LocalPath){//Uploads a file
	//DebugFunctionTrace($FunctionName="FileUpload", $Parameter=array("RemoteFile"=>$RemoteFile, "LocalPath"=>$LocalPath), $UseURLDebugFlag=true);

        if($_FILES[$RemoteFile]["name"]!=""){
            if(!file_exists($LocalPath)){@mkdir($LocalPath,0777);}
            @chmod($LocalPath,0777);
            if(file_exists($LocalPath.$_FILES[$RemoteFile]["name"])){
                $NewName = md5(uniqid(rand(0, 1000),1))."_".$_FILES[$RemoteFile]["name"];
            }else{$NewName = $_FILES[$RemoteFile]["name"];}

	        move_uploaded_file($_FILES[$RemoteFile]["tmp_name"], $LocalPath.$NewName);
	        return $NewName;
        }else{return "";}
    }
    
	//Process the upload of a user posted file, delete the existing file if requested
    function ProcessUpload($FieldName, $UploadPath){
		
		//DebugFunctionTrace($FunctionName="ProcessUpload", $Parameter=array("FieldName"=>$FieldName, "UploadPath"=>$UploadPath), $UseURLDebugFlag=true);

        $Document=FileUpload($FieldName."New", $UploadPath);
        if(($_POST[$FieldName]!="" and $Document!="") or isset($_POST[$FieldName."Delete"])){@unlink($UploadPath.$_POST[$FieldName]);}
        if($_POST[$FieldName]!="" and $Document=="" and !isset($_POST[$FieldName."Delete"])){$Document=$_POST[$FieldName];}
        
        return $Document;
	}
?>
