<?
	//Return the static content. It automatically determines the current language code
	//  $StaticContentName {string} =   Name of the content to return
	function StaticContent($StaticContentName){
		
	    DebugFunctionTrace($FunctionName="StaticContent", $Parameter=array("StaticContentName"=>$StaticContentName), $UseURLDebugFlag=true);

		global $Application;
	    $StaticContent=SQL_Select($Entity="StaticContent", $Where="SC.StaticContentName = '$StaticContentName' AND L.LanguageCode = '{$_REQUEST["LanguageCode"]}'", $OrderBy="SC.StaticContentName", $SingleRow=true);
        $StaticContentHTML=$StaticContentID=$StaticContentUUID="";
	    if(count($StaticContent)>0){
	        $StaticContentHTML.=CRLFToBR($StaticContent["StaticContent"]);
	        $StaticContentID=$StaticContent["StaticContentID"];
	        $StaticContentUUID=$StaticContent["StaticContentUUID"];
		}
	    if($_SESSION["UserTypeID"]==$Application["UserTypeIDAdministrator"])$StaticContentHTML.=" <a href=\"#\" class=\"StaticContentControlButton\" onclick=\"PopUpStaticContentEditor('$StaticContentName')\">EDIT CONTENT</a>";
	    //&&isset($_REQUEST["StaticContentEdit"])
	    //if(isset($_REQUEST["StaticContentEdit"]))$StaticContentHTML.=" <a href=\"#\" class=\"StaticContentControlButton\" onclick=\"PopUpStaticContentEditor('$StaticContentName')\">EDIT CONTENT</a>";
	    return $StaticContentHTML;
	}

?>