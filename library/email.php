<?
	/*
		Template:   email.php
		Purpose:    Email related functions
		Risk:       Normal
		Author:     Shahriar Kabir (SKJoy2001@Yahoo.Com)
		Date:       February 1, 2004
	*/

	//$EmailExtraHeaderDelimeter=chr(13).chr(10); //For Windows server
	$EmailExtraHeaderDelimeter="\r\n";          //For LINUX server

	//Send email with custom header information in the easy way
	function SendMail($ToEmail="", $Subject="", $Body="", $FromName="", $FromEmail = "", $ReplyToName="", $ReplyToEmail="", $ExtraHeaderParameters="", $Debug=false){
		DebugFunctionTrace($FunctionName="SendMail", $Parameter=array("ToEmail"=>$ToEmail, "Subject"=>$Subject, "Body"=>$Body, "FromName"=>$FromName, "FromEmail"=>$FromEmail, "ReplyToName"=>$ReplyToName, "ReplyToEmail"=>$ReplyToEmail, "ExtraHeaderParameters"=>$ExtraHeaderParameters, "Debug"=>$Debug), $UseURLDebugFlag=true);

		global $EmailExtraHeaderDelimeter, $Application;
		
		if($Debug)print "
		    function SendMail(\$ToEmail='$ToEmail', \$Subject='$Subject', \$Body='$Body', \$FromName='$FromName', \$FromEmail = '$FromEmail', \$ReplyToName='$ReplyToName', \$ReplyToEmail='$ReplyToEmail', \$ExtraHeaderParameters='$ExtraHeaderParameters', \$Debug=$Debug){<br>
		    <br>
		    }
		";

		if($FromName=="")$FromName=$Application["Title"];
		if($FromEmail=="")$FromEmail=$Application["EmailSupport"];

		if($ReplyToName=="")$ReplyToName=$FromName;
		if($ReplyToEmail=="")$ReplyToEmail=$FromEmail;

		$ExtraHeader="";
		$ExtraHeader.="From: $FromName <$FromEmail>".$EmailExtraHeaderDelimeter;
		$ExtraHeader.="Reply-To: $ReplyToName <$ReplyToEmail>".$EmailExtraHeaderDelimeter;

		//You must supply the full extra header information, not just the values, and also the delimeters for multiple headers
		if($ExtraHeaderParameters!=""){$ExtraHeader=$ExtraHeader.$ExtraHeaderParameters.$EmailExtraHeaderDelimeter;}

		$ExtraHeader=$ExtraHeader."MIME-Version: 1.0".$EmailExtraHeaderDelimeter;
		$ExtraHeader=$ExtraHeader."X-Mailer: ".$Application["Title"]."/1.0".$EmailExtraHeaderDelimeter;
		$ExtraHeader=$ExtraHeader."Content-Type: text/html; charset=\"iso-8859-1\"";

		@mail($ToEmail, $Subject, "<html><body>".$Body."</body></html>", $ExtraHeader);
	}

	function IsValidEmailAddress($EmailAddress) {
		DebugFunctionTrace($FunctionName="IsValidEmailAddress", $Parameter=array("EmailAddress"=>$EmailAddress), $UseURLDebugFlag=true);

		$Valid = true;
		if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $EmailAddress))$Valid = false;
		return $Valid;
	}
?>