<?
	//Input type TEXT
	function CTL_InputText($Name, $DefaultValue="", $Title="", $Size="", $Class="FormInputText", $Style="", $ReadOnly=false, $Debug=false){
		
		DebugFunctionTrace($FunctionName="CTL_InputText", $Parameter=array("Name"=>$Name, "DefaultValue"=>$DefaultValue, "Title"=>$Title, "Size"=>$Size, "Class"=>$Class, "Style"=>$Style, "ReadOnly"=>$ReadOnly, "Debug"=>$Debug), $UseURLDebugFlag=true);

	    SetFormVariable($Name, $DefaultValue, $SetErrorFlag=true, $UseRequestVariable=true, $Debug);
	    $ReadOnlyHTML="";
	    if($ReadOnly)$ReadOnlyHTML=" readonly=\"true\"";
		$HTML="<input type=\"text\" name=\"$Name\" value=\"{$_POST[$Name]}\" title=\"$Title\" size=\"$Size\"$ReadOnlyHTML class=\"$Class\" style=\"$Style\">";
		if($Debug)print "
		    CTL_InputText(\$Name='$Name', \$DefaultValue='$DefaultValue', \$Title='$Title', \$Size=$Size, \$Class='$Class', \$Style='$Style', \$ReadOnly=$ReadOnly, \$Debug=$Debug){
			}
		    <hr>
		";
		return $HTML;
	}
	//TEXT AREA
	function CTL_InputTextArea($Name, $Value="", $Columns=80, $Rows=15, $Class="", $Style="", $ReadOnly=false){

		DebugFunctionTrace($FunctionName="CTL_InputTextArea", $Parameter=array("Name"=>$Name, "Value"=>$Value, "Columns"=>$Columns, "Rows"=>$Rows, "Class"=>$Class, "Style"=>$Style, "ReadOnly"=>$ReadOnly), $UseURLDebugFlag=true);


	    SetFormVariable($Name, $Value, $SetErrorFlag=true, $UseRequestVariable=true);
	    $ReadOnlyHTML="";
	    if($ReadOnly)$ReadOnlyHTML=" readonly";
		$HTML="<textarea name=\"$Name\" cols=\"$Columns\" rows=\"$Rows\" class=\"$Class\"$ReadOnlyHTML style=\"$Style\">{$_POST[$Name]}</textarea>";
		return $HTML;
	}
	//Input type PASSWORD
	function CTL_InputPassword($Name, $DefaultValue="", $Title="", $Size="", $Class="FormInputPassword", $Style="", $Debug=false){

		DebugFunctionTrace($FunctionName="CTL_InputPassword", $Parameter=array("Name"=>$Name, "DefaultValue"=>$DefaultValue, "Title"=>$Title, "Size"=>$Size, "Class"=>$Class, "Style"=>$Style, "Debug"=>$Debug), $UseURLDebugFlag=true);


	    SetFormVariable($Name, $DefaultValue, $SetErrorFlag=true, $UseRequestVariable=true);
		$HTML="<input type=\"password\" name=\"$Name\" value=\"\" title=\"$Title\" size=\"$Size\" class=\"$Class\" style=\"$Style\">";
		return $HTML;
	}
	//Input type CHECKBOX
	function CTL_InputCheck($Name, $Value=-1, $Title="", $Class="FormInputCheck", $Style=""){
		
		DebugFunctionTrace($FunctionName="CTL_InputCheck", $Parameter=array("Name"=>$Name, "Value"=>$Value, "Title"=>$Title, "Class"=>$Class, "Style"=>$Style), $UseURLDebugFlag=true);

	    SetFormVariable($Name, $Value, $SetErrorFlag=true, $UseRequestVariable=true);
		$HTML="<input type=\"checkbox\" name=\"$Name\" value=\"{$_POST[$Name]}\" title=\"$Title\" class=\"$Class\" style=\"$Style\"";
	    if(isset($_POST[$Name])&&$_POST[$Name]>(-1))$HTML.=" checked";
		$HTML.=">";
		return $HTML;
	}
	//Input type RADIO
	function CTL_InputRadio($Name, $Value, $ValueSelected="", $Title="", $Class="FormInputRadio", $Style=""){

		DebugFunctionTrace($FunctionName="CTL_InputRadio", $Parameter=array("Name"=>$Name, "Value"=>$Value, "ValueSelected"=>$ValueSelected, "Title"=>$Title, "Class"=>$Class, "Style"=>$Style), $UseURLDebugFlag=true);

	    SetFormVariable($Name, $ValueSelected, $SetErrorFlag=true, $UseRequestVariable=true);
		$HTML="<input type=\"radio\" name=\"$Name\" value=\"$Value\" title=\"$Title\" class=\"$Class\" style=\"$Style\"";
	    if($_POST[$Name]==$Value)$HTML.=" checked";
		$HTML.=">";
		return $HTML;
	}
	//Input type RADIO set
	function CTL_InputRadioSet($VariableName, $Captions, $Values, $CurrentValue, $Class="FormInputRadio", $Style=""){
		DebugFunctionTrace($FunctionName="CTL_InputRadioSet", $Parameter=array("VariableName"=>$VariableName, "Captions"=>$Captions, "Values"=>$Values, "CurrentValue"=>$CurrentValue, "Class"=>$Class, "Style"=>$Style), $UseURLDebugFlag=true);

		/*
			$VariableName: Name of the variable to hold the selection value
			$Captions: Array of captions
	        $Values: Array of values of datatype of string or number
			$CurrentValue: Default value to set with if no value is found
			$Class: Style class to use with the appearance
			$Style: Custom style to force the appearance with
	    */
	    SetFormVariable($VariableName, $CurrentValue, $SetErrorFlag=true, $UseRequestVariable=true);
	    $HTML="";
	    $ValueCounter=-1;
	    foreach($Values as $ThisValue){
	        $ValueCounter++;
	        $Selected="";
	        if($ThisValue==$_POST[$VariableName])$Selected=" checked";
	        $HTML.="<input type=\"radio\" name=\"$VariableName\" value=\"$ThisValue\" class=\"$Class\" style=\"$Style\"$Selected> {$Captions[$ValueCounter]} ";
		}
		/*
		print "\$_POST[\"$VariableName\"] = {$_POST[$VariableName]}<hr>";
		print "\$CurrentValue = $CurrentValue<hr>";
		*/
		return $HTML;
	}
	//Input type BUTTON
	function CTL_InputButton($Name="", $Value="", $Title="", $Size="", $Class="FormInputButton", $Style="", $OnClick=""){

		DebugFunctionTrace($FunctionName="CTL_InputButton", $Parameter=array("Name"=>$Name, "Value"=>$Value, "Title"=>$Title, "Size"=>$Size, "Class"=>$Class, "Style"=>$Style, "OnClick"=>$OnClick), $UseURLDebugFlag=true);

		$HTML="<input type=\"button\" name=\"$Name\" value=\"$Value\" title=\"$Title\" size=\"$Size\" class=\"$Class\" style=\"$Style\" OnClick=\"$OnClick\">";
		return $HTML;
	}
	//Input type SUBMIT
	function CTL_InputSubmit($Name="", $Value="Post", $Title="", $Size="", $Class="FormInputButton", $Style="", $OnClick=""){
		
		DebugFunctionTrace($FunctionName="CTL_InputSubmit", $Parameter=array("Name"=>$Name, "Value"=>$Value, "Title"=>$Title, "Size"=>$Size, "Class"=>$Class, "Style"=>$Style, "OnClick"=>$OnClick), $UseURLDebugFlag=true);


		$HTML="<input type=\"submit\" name=\"$Name\" value=\"$Value\" title=\"$Title\" size=\"$Size\" class=\"$Class\" style=\"$Style\" onclick=\"$OnClick\">";
		return $HTML;
	}
	//Input type RESET
	function CTL_InputReset($Name, $Value="", $Title="", $Size="", $Class="FormInputButton", $Style=""){

		DebugFunctionTrace($FunctionName="CTL_InputReset", $Parameter=array("Name"=>$Name, "Value"=>$Value, "Title"=>$Title, "Size"=>$Size, "Class"=>$Class, "Style"=>$Style), $UseURLDebugFlag=true);

		$HTML="<input type=\"reset\" name=\"$Name\" value=\"$Value\" title=\"$Title\" size=\"$Size\" class=\"$Class\" style=\"$Style\">";
		return $HTML;
	}

	//Input type HIDDEN
	function CTL_InputHidden($Name, $Value=""){

		DebugFunctionTrace($FunctionName="CTL_InputHidden", $Parameter=array("Name"=>$Name, "Value"=>$Value), $UseURLDebugFlag=true);

	    SetFormVariable($Name, $Value, $SetErrorFlag=true, $UseRequestVariable=true);
		$HTML="<input type=\"hidden\" name=\"$Name\" value=\"{$_POST[$Name]}\">";
		return $HTML;
	}

	//SELECT
	function CTL_Combo($Name="", $Values, $Captions, $IncludeBlankItem=false, $CurrentValue, $BlankItemCaption="", $Class="", $Style=""){
		DebugFunctionTrace($FunctionName="CTL_Combo", $Parameter=array("Name"=>$Name, "Values"=>$Values, "Captions"=>$Captions, "IncludeBlankItem"=>$IncludeBlankItem, "CurrentValue"=>$CurrentValue, "BlankItemCaption"=>$BlankItemCaption, "Class"=>$Class, "Style"=>$Style), $UseURLDebugFlag=true);

		SetFormVariable($Name, $CurrentValue, $SetErrorFlag=true, $UseRequestVariable=true);
		$HTML="<select name=\"$Name\" class=\"$Class\" style=\"$Style\">";
		if($IncludeBlankItem)$HTML.="<option value=\"\">$BlankItemCaption</option>";
		foreach($Values as $Value){
			$HTML.="<option value=\"$Value\"";
			if($Value==$_POST[$Name])$HTML.=" selected";
			$HTML.=">".$Captions[array_search($Value, $Values)]."</option>";
		}
		$HTML.="</select>";
		return $HTML;
	}
	
	//Image
	function CTL_Image($ImageFile, $Height=0, $Width=0, $Class="", $Nothing=false){
		DebugFunctionTrace($FunctionName="CTL_Image", $Parameter=array("ImageFile"=>$ImageFile, "Height"=>$Height, "Width"=>$Width, "Class"=>$Class, "Nothing"=>$Nothing), $UseURLDebugFlag=true);

		global $Application;
	    $ImageFile=$Application["UploadPath"].$ImageFile;
	    if(!file_exists($ImageFile) or $ImageFile==$Application["UploadPath"])$ImageFile="./theme/{$_REQUEST["Theme"]}/image/other/noimage.gif";
	    $HeightHTML=$WidthHTML="";
	    if($Height>0)$HeightHTML=" height=\"$Height\"";
	    if($Width>0)$WidthHTML=" width=\"$Width\"";
	    if(!$Nothing or $ImageFile!="./theme/{$_REQUEST["Theme"]}/image/other/noimage.gif"){
	    	return "<img src=\"$ImageFile\"".$HeightHTML.$WidthHTML." class=\"$Class\">";
		}else{
		    return "";
		}
	}

	function CTL_HeightSelector($HeightSelectorName, $HeightSelected="", $HeightStart=42, $HeightStop=84, $Class="DataFormInput", $ShowFeet=true, $ShowInch=true, $ShowCM=true){

		DebugFunctionTrace($FunctionName="CTL_HeightSelector", $Parameter=array("HeightSelectorName"=>$HeightSelectorName, "HeightSelected"=>$HeightSelected, "HeightStart"=>$HeightStart, "HeightStop"=>$HeightStop, "Class"=>$Class, "ShowFeet"=>$ShowFeet, "ShowInch"=>$ShowInch, "ShowCM"=>$ShowCM), $UseURLDebugFlag=true);



		/*Build HTML code for a height picker by Feet-Inch-Cm list
		$HeightSelectorName	= Outputs a list of heights to chose from
		$HeightSelected		= Selected height
		$HeightStart		= Height to start the list from
		$HeightStop			= Height to stop the list at
		$Class				= CSS class to be used for the control, if NULL, "FormTextInput" is used
		Date: Sunday, June 05, 2005				Developer: Shahriar Kabir Joy (SKJoy2001@Yahoo.Com)*/
		if($Class==""){$Class="DataFormInput";}
    	$HTML_Code="<select name=$HeightSelectorName class=$Class>";
		for($Counter=$HeightStart;$Counter<=$HeightStop;$Counter++){
			$HTML_Code=$HTML_Code."<option value=\"$Counter\"";
			if($HeightSelected==$Counter){$HTML_Code=$HTML_Code." selected";}
			$HeightFeet=round($Counter/12, 2)." ft";
			$HeightInch="$Counter inch";
			$HeightCM=round($Counter*2.54, 0)." cm";
			$HeightOptionHTML="";
			if($ShowFeet)if($HeightOptionHTML==""){$HeightOptionHTML.=$HeightFeet;}else{$HeightOptionHTML.=" = $HeightFeet";}
			if($ShowInch)if($HeightOptionHTML==""){$HeightOptionHTML.=$HeightInch;}else{$HeightOptionHTML.=" = $HeightInch";}
			if($ShowCM)if($HeightOptionHTML==""){$HeightOptionHTML.=$HeightCM;}else{$HeightOptionHTML.=" = $HeightCM";}
			$HTML_Code=$HTML_Code.">$HeightOptionHTML</option>";
		}
		$HTML_Code=$HTML_Code."</select>";
		return $HTML_Code;
	}

	function CTL_WeightSelector($WeightSelectorName, $WeightSelected="", $WeightStart=50, $WeightStop=250, $Class="DataFormInput"){

	DebugFunctionTrace($FunctionName="CTL_WeightSelector", $Parameter=array("WeightSelectorName"=>$WeightSelectorName, "WeightSelected"=>$WeightSelected, "WeightStart"=>$WeightStart, "WeightStop"=>$WeightStop, "Class"=>$Class), $UseURLDebugFlag=true);

		/*Build HTML code for a height picker by Feet-Inch-Cm list

		$WeightSelectorName	= Outputs a list of weights to chose from
		$WeightSelected		= Selected weight
		$WeightStart		= Weight to start the list from
		$WeightStop			= Weight to stop the list at
		$Class				= CSS class to be used for the control, if NULL, "FormTextInput" is used

		Date: Sunday, June 05, 2005				Developer: Shahriar Kabir Joy (SKJoy2001@Yahoo.Com)*/

		if($Class==""){$Class="DataFormInput";}

    	$HTML_Code="<select name=$WeightSelectorName class=$Class>";
		for($Counter=$WeightStart;$Counter<=$WeightStop;$Counter++){
			$HTML_Code=$HTML_Code."<option value=\"$Counter\"";
			if($WeightSelected==$Counter){$HTML_Code=$HTML_Code." selected";}
			$HTML_Code=$HTML_Code.">$Counter lbs = ".round($Counter/2.2, 0)." kg</option>";
		}
		$HTML_Code=$HTML_Code."</select>";

		return $HTML_Code;
	}

	function CTL_TimeSelector($TimeSelectorName, $HourSelected="01", $MinuteSelected="00", $SecondSelected="00", $Class="DataFormInput"){

		DebugFunctionTrace($FunctionName="CTL_TimeSelector", $Parameter=array("TimeSelectorName"=>$TimeSelectorName, "HourSelected"=>$HourSelected, "MinuteSelected"=>$MinuteSelected, "SecondSelected"=>$SecondSelected, "Class"=>$Class), $UseURLDebugFlag=true);

		/*Build HTML code for a time picker by Hour-Minute-Second list

		$TimeSelectorName	= Outputs 3 form controls as $TimeSelectorName_Hour, $TimeSelectorName_Minute & $TimeSelectorName_Second
		$HourSelected		= Selected hour
		$MinuteSelected		= Selected minute
		$SecondSelected		= Selected second
		$Class				= CSS class to be used for the control, if NULL, "FormTextInput" is used

		Date: Sunday, June 05, 2005				Developer: Shahriar Kabir Joy (SKJoy2001@Yahoo.Com)*/

		if($Class==""){$Class="DataFormInput";}

		$HTML_Code="<select name=".$TimeSelectorName."Hour class=$Class>";
		for($Counter=0;$Counter<=23;$Counter++){
			$HTML_Code=$HTML_Code."<option value=".$Counter;
			if($Counter==$HourSelected){$HTML_Code=$HTML_Code." selected";}
            $HTML_Code=$HTML_Code.">".str_pad($Counter, 2, "0", STR_PAD_LEFT)."</option>";
		}
		$HTML_Code=$HTML_Code."</select>";

		$HTML_Code=$HTML_Code."<select name=".$TimeSelectorName."Minute class=$Class>";
		for($Counter=0;$Counter<=59;$Counter++){
			$HTML_Code=$HTML_Code."<option value=".$Counter;
			if($Counter==$MinuteSelected){$HTML_Code=$HTML_Code." selected";}
            $HTML_Code=$HTML_Code.">".str_pad($Counter, 2, "0", STR_PAD_LEFT)."</option>";
		}
		$HTML_Code=$HTML_Code."</select>";

		$HTML_Code=$HTML_Code."<select name=".$TimeSelectorName."Second class=$Class>";
		for($Counter=0;$Counter<=59;$Counter++){
			$HTML_Code=$HTML_Code."<option value=".$Counter;
			if($Counter==$SecondSelected){$HTML_Code=$HTML_Code." selected";}
            $HTML_Code=$HTML_Code.">".str_pad($Counter, 2, "0", STR_PAD_LEFT)."</option>";
		}
		$HTML_Code=$HTML_Code."</select>";

		return $HTML_Code;
	}

	function CTL_DateSelector($DateSelectorName, $SelectedDate="", $YearHalfSpan=50, $Class="", $Years=true, $Months=true, $Days=true){

	DebugFunctionTrace($FunctionName="CTL_DateSelector", $Parameter=array("DateSelectorName"=>$DateSelectorName, "SelectedDate"=>$SelectedDate, "YearHalfSpan"=>$YearHalfSpan, "Class"=>$Class, "Years"=>$Years, "Months"=>$Months, "Days"=>$Days), $UseURLDebugFlag=true);

		/*Build HTML code for a date picker by Year-Month-Day list

		$DateSelectorName	= Outputs 3 form controls as $DateSelectorName_Year, $DateSelectorName_Month & $DateSelectorName_Day
		$SelectedDate		= Selected date (YYYY-MM-DD)
		$YearHalfSpan		= Half value of the years to be ranged from YearSelected

		Date: Sunday, June 05, 2005				Developer: Shahriar Kabir Joy (SKJoy2001@Yahoo.Com)*/
		
		if($SelectedDate==""||$SelectedDate=="0000-00-00")$SelectedDate=date("Y-m-d");

	    SetFormVariable($VariableName=$DateSelectorName, $CurrentValue=$SelectedDate, $SetErrorFlag=true, $UseRequestVariable=true);
/*
	    SetFormVariable($VariableName=$DateSelectorName."Year", $CurrentValue=date("Y", strtotime($SelectedDate)), $SetErrorFlag=true, $UseRequestVariable=true);
	    SetFormVariable($VariableName=$DateSelectorName."Month", $CurrentValue=date("m", strtotime($SelectedDate)), $SetErrorFlag=true, $UseRequestVariable=true);
	    SetFormVariable($VariableName=$DateSelectorName."Day", $CurrentValue=date("d", strtotime($SelectedDate)), $SetErrorFlag=true, $UseRequestVariable=true);
*/
	    SetFormVariable($VariableName=$DateSelectorName."Year", $CurrentValue=substr($SelectedDate, 0, 4), $SetErrorFlag=true, $UseRequestVariable=true);
	    SetFormVariable($VariableName=$DateSelectorName."Month", $CurrentValue=substr($SelectedDate, 5, 2), $SetErrorFlag=true, $UseRequestVariable=true);
	    SetFormVariable($VariableName=$DateSelectorName."Day", $CurrentValue=substr($SelectedDate, 8, 2), $SetErrorFlag=true, $UseRequestVariable=true);

        $YearSelected= $_POST[$DateSelectorName."Year"];
        $MonthSelected=$_POST[$DateSelectorName."Month"];
        $DaySelected=  $_POST[$DateSelectorName."Day"];

        $strDateSelector="";

		if($Years){
	        $strDateSelector=$strDateSelector."<select name=\"".$DateSelectorName."Year\" class=\"$Class\">";
			for($Counter=$YearSelected-$YearHalfSpan;$Counter<=$YearSelected+$YearHalfSpan;$Counter++){
				$strDateSelector=$strDateSelector."<option value=\"".$Counter."\"";
				if($Counter==$YearSelected){$strDateSelector=$strDateSelector." selected";}
	            $strDateSelector=$strDateSelector.">".$Counter."</option>";
			}
			$strDateSelector=$strDateSelector."</select>";
		}else{$strDateSelector.="<input type=\"hidden\" name=\"$DateSelectorName\" value=\"$YearSelected\">";}

		if($Months){
	        $strDateSelector=$strDateSelector."<select name=\"".$DateSelectorName."Month\" class=\"DataFormInput\">";
			$strDateSelector=$strDateSelector."<option value=\"01\"";
			if($MonthSelected==1){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">January</option>";
			$strDateSelector=$strDateSelector."<option value=\"02\"";
			if($MonthSelected==2){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">February</option>";
			$strDateSelector=$strDateSelector."<option value=\"03\"";
			if($MonthSelected==3){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">March</option>";
			$strDateSelector=$strDateSelector."<option value=\"04\"";
			if($MonthSelected==4){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">April</option>";
			$strDateSelector=$strDateSelector."<option value=\"05\"";
			if($MonthSelected==5){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">May</option>";
			$strDateSelector=$strDateSelector."<option value=\"06\"";
			if($MonthSelected==6){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">June</option>";
			$strDateSelector=$strDateSelector."<option value=\"07\"";
			if($MonthSelected==7){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">July</option>";
			$strDateSelector=$strDateSelector."<option value=\"08\"";
			if($MonthSelected==8){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">August</option>";
			$strDateSelector=$strDateSelector."<option value=\"09\"";
			if($MonthSelected==9){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">September</option>";
			$strDateSelector=$strDateSelector."<option value=\"10\"";
			if($MonthSelected==10){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">October</option>";
			$strDateSelector=$strDateSelector."<option value=\"11\"";
			if($MonthSelected==11){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">November</option>";
			$strDateSelector=$strDateSelector."<option value=\"12\"";
			if($MonthSelected==12){$strDateSelector=$strDateSelector." selected";}
	        $strDateSelector=$strDateSelector.">December</option>";
			$strDateSelector=$strDateSelector."</select>";
		}else{$strDateSelector.="<input type=\"hidden\" name=\"$DateSelectorName\" value=\"$MonthSelected\">";}

		if($Days){
	        $strDateSelector=$strDateSelector."<select name=\"".$DateSelectorName."Day\" class=\"DataFormInput\">";
			for($Counter=1;$Counter<=31;$Counter++){
				$strDateSelector=$strDateSelector."<option value=\"".$Counter."\"";
				if($Counter==$DaySelected){$strDateSelector=$strDateSelector." selected";}
	            $strDateSelector=$strDateSelector.">".$Counter."</option>";
			}
			$strDateSelector=$strDateSelector."</select>";
		}else{$strDateSelector.="<input type=\"hidden\" name=\"$DateSelectorName\" value=\"$DaySelected\">";}

		return $strDateSelector;
    }

	//Date range selection control
    function CTL_DateRangeSelector(
        $DateRangeSelectorName,
        $FromYear=0,
        $FromMonth=0,
        $FromDay=0,
        $FromCaption="From",
        $ToYear=0,
        $ToMonth=0,
        $ToDay=0,
        $ToCaption="to",
        $YearHalfSpan=50,
        $Class="",
        $FromDate="",
        $ToDate=""
    ){

       DebugFunctionTrace($FunctionName="CTL_DateRangeSelector", $Parameter=array("DateRangeSelectorName"=>$DateRangeSelectorName, "FromYear"=>$FromYear, "FromMonth"=>$FromMonth, "FromDay"=>$FromDay, "FromCaption"=>$FromCaption, "ToYear"=>$ToYear, "ToMonth"=>$ToMonth, "ToDay"=>$ToDay, "ToCaption"=>$ToCaption, "YearHalfSpan"=>$YearHalfSpan, "Class"=>$Class, "FromDate"=>$FromDate, "ToDate"=>$ToDate), $UseURLDebugFlag=true);



        $HTML_Code="
            $FromCaption ".CTL_DateSelector($DateRangeSelectorName."From", $FromYear, $FromMonth, $FromDay, $YearHalfSpan, $Class, $FromDate)."
            $ToCaption ".CTL_DateSelector($DateRangeSelectorName."To", $ToYear, $ToMonth, $ToDay, $YearHalfSpan, $Class, $ToDate)."
        ";

        return $HTML_Code;
    }

	//A file upload control with DELETE EXISTING link & download link
    function CTL_FileUpload($ControlName, $CurrentFile="", $AllowDelete=true, $Class="FormTextInput", $Size=50){


    	global $Application;

		DebugFunctionTrace($FunctionName="CTL_FileUpload", $Parameter=array("ControlName"=>$ControlName, "CurrentFile"=>$CurrentFile, "AllowDelete"=>$AllowDelete, "Class"=>$Class, "Size"=>$Size), $UseURLDebugFlag=true);

	    SetFormVariable($ControlName, $CurrentFile, $SetErrorFlag=true, $UseRequestVariable=true);

    	if(!$Class)$Class="FormTextInput";

    	$DocumentFile=$Application["UploadPath"].$CurrentFile;
    	if(!file_exists($Application["UploadPath"].$CurrentFile) or !$CurrentFile){$DocumentFile="./theme/".$_REQUEST["Theme"]."/image/noimage.gif";}

    	$HTML_Code="<input type=\"file\" name=\"".$ControlName."New\" size=\"$Size\" class=\"$Class\"><br>";
    	if(file_exists($Application["UploadPath"].$CurrentFile)&&$CurrentFile){$HTML_Code.="<a href=\"".$Application["UploadPath"].$CurrentFile."\" class=\"FormTextLink\" title=\" Download \"><img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid/datagrid_action_download.gif\" border=\"0\">&nbsp;Download</a>&nbsp;";}
    	if(file_exists($Application["UploadPath"].$CurrentFile) and $CurrentFile){$HTML_Code.=CTL_InputCheck($ControlName."Delete")." Delete";}
    	$HTML_Code.="<input type=\"hidden\" name=\"$ControlName\" value=\"$CurrentFile\">";

		return $HTML_Code;
	}

	//An image upload control with DELETE EXISTING link & preview
    function CTL_ImageUpload($ControlName, $CurrentImage="", $AllowDelete=true, $Class="FormTextInput", $ThumbnailHeight=100, $ThumbnailWidth=0, $Preview=true, $Size=50){
	    DebugFunctionTrace($FunctionName="CTL_ImageUpload", $Parameter=array("ControlName"=>$ControlName, "CurrentImage"=>$CurrentImage, "AllowDelete"=>$AllowDelete, "Class"=>$Class, "ThumbnailHeight"=>$ThumbnailHeight, "ThumbnailWidth"=>$ThumbnailWidth, "Preview"=>$Preview, "Size"=>$Size), $UseURLDebugFlag=true); 

		global $Application;
	    SetFormVariable($ControlName, $CurrentImage, $SetErrorFlag=true, $UseRequestVariable=true);
    	if(!$Class)$Class="FormTextInput";
    	$ImageFile=$Application["UploadPath"].$_POST[$ControlName];
    	if(!$CurrentImage or !file_exists($ImageFile)){$ImageFile="./theme/".$_REQUEST["Theme"]."/image/other/noimage.gif";}
    	$HTML_Code="<input type=\"file\" name=\"".$ControlName."New\" accept=\"image/png, image/gif, image/jpeg, image/bmp\" size=\"$Size\" class=\"$Class\"><br>";
    	if($Preview){
	    	$HTML_Code.="<img src=\"$ImageFile\" border=\"";
        	if(!$CurrentImage||!file_exists($ImageFile)){$HTML_Code.="0";}else{$HTML_Code.="1";}
	    	if($ThumbnailWidth){$HTML_Code.=" width=$ThumbnailWidth";}else{$HTML_Code.="\" height=\"$ThumbnailHeight";}
	    	$HTML_Code.="\">";
		}
    	if($CurrentImage and file_exists($ImageFile))$HTML_Code.="<br>".CTL_InputCheck($ControlName."Delete")." Delete current picture";
    	$HTML_Code.="<input type=\"hidden\" name=\"$ControlName\" value=\"$CurrentImage\">";
		return $HTML_Code;
	}

	//A control to make a custom window/panel/box with Caption, icon, etc.
	function CTL_Window($Title="", $Content="", $Width="", $Icon="system", $Template=""){

		 DebugFunctionTrace($FunctionName="CTL_Window", $Parameter=array("Title"=>$Title, "Content"=>$Content, "Width"=>$Width, "Icon"=>$Icon, "Template"=>$Template), $UseURLDebugFlag=true); 

		if($Icon){
			$HTML_Icon="
								<td width=\"1\" valign=\"top\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/window/".$Template."window_icon_".$Icon.".gif\"></td>
								<td width=\"1\">&nbsp;&nbsp;</td>
			";
		}else{$HTML_Icon="";}
		$HTML="
		    <div align=\"center\">
				<table class=\"".$Template."WindowTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					<tr class=\"".$Template."WindowRowTop\">
						<td class=\"".$Template."WindowRowTopCellLeft\"></td>
						<td class=\"".$Template."WindowRowTopCellCenter\">
							<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
								<tr>
									<td width=\"1\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/window/".$Template."window_icon.gif\"></td>
									<td width=\"1\">&nbsp;</td>
									<td class=\"".$Template."WindowTitle\">$Title</td>
								</tr>
							</table>
						</td>
						<td class=\"".$Template."WindowRowTopCellRight\"></td>
					</tr>
					<tr class=\"".$Template."WindowRowMiddle\">
						<td class=\"".$Template."WindowRowMiddleCellLeft\"></td>
						<td class=\"".$Template."WindowRowMiddleCellCenter\" width=\"$Width\">
							<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
								<tr>
									".$HTML_Icon."
									<td class=\"".$Template."WindowContent\">$Content</td>
								</tr>
							</table>
						</td>
						<td class=\"".$Template."WindowRowMiddleCellRight\"></td>
					</tr>
					<tr class=\"".$Template."WindowRowBottom\">
						<td class=\"".$Template."WindowRowBottomCellLeft\"></td>
						<td class=\"".$Template."WindowRowBottomCellCenter\"></td>
						<td class=\"".$Template."WindowRowBottomCellRight\"></td>
					</tr>
				</table>
			</div>
		";
		return $HTML;
	}
	//Datagrid control!
	function CTL_Datagrid(
		$Entity,
		$ColumnName,
		$ColumnTitle,
		$ColumnAlign,
		$ColumnType,
		$Rows,
		$SearchHTML,
		$ControlHTML,
		$AdditionalLinks=array(),
		$AdditionalActionParameter="",
		$ActionLinks=true,
		$SearchPanel=true,
		$ControlPanel=true,
		$CheckBox=true,
		$EntityAlias="",
		$SortLinkExtraParameter="",
		$ControlPanelFormActionExtraParameter=""
	){
		
		 DebugFunctionTrace($FunctionName="CTL_Datagrid", $Parameter=array("Entity"=>$Entity, "ColumnName"=>$ColumnName, "ColumnTitle"=>$ColumnTitle, "ColumnAlign"=>$ColumnAlign, "ColumnType"=>$ColumnType, "Rows"=>$Rows, "SearchHTML"=>$SearchHTML, "ControlHTML"=>$ControlHTML, "AdditionalLinks"=>$AdditionalLinks, "AdditionalActionParameter"=>$AdditionalActionParameter, "ActionLinks"=>$ActionLinks, "SearchPanel"=>$SearchPanel, "ControlPanel"=>$ControlPanel, "CheckBox"=>$CheckBox, "EntityAlias"=>$EntityAlias, "SortLinkExtraParameter"=>$SortLinkExtraParameter, "ControlPanelFormActionExtraParameter"=>$ControlPanelFormActionExtraParameter), $UseURLDebugFlag=true);

	    global $Application;
	    $EntityLower=strtolower($Entity);
	    if($EntityAlias=="")$EntityAlias=$Entity;
	    $EntityAliasLower=strtolower($EntityAlias);
		$Title_Search="Search/filter $EntityAliasLower record(s)";
		if(!isset($_REQUEST["SortBy"]))$_REQUEST["SortBy"]=$ColumnName[0];
		if(!isset($_REQUEST["SortType"]))$_REQUEST["SortType"]="ASC";
		if($_REQUEST["SortType"]=="ASC"){$ReverseSortType="DESC";}else{$ReverseSortType="ASC";}
	    SetFormvariable("RecordShowFrom", 1);
	    SetFormvariable("RecordShowUpTo", 20);
		$FormActionURL=ApplicationURL($Script=$_REQUEST["Script"], "RecordShowFrom={$_POST["RecordShowFrom"]}&RecordShowUpTo={$_POST["RecordShowUpTo"]}&SortBy={$_REQUEST["SortBy"]}&SortType={$_REQUEST["SortType"]}&$ControlPanelFormActionExtraParameter");
		//if($AdditionalActionParameter!="")$FormActionURL.="&$AdditionalActionParameter";
		$HTML_Search="
		        <form name=\"frmDataGridSearch$Entity\" action=\"$FormActionURL\" method=\"post\" enctype=\"multipart form/data\">
					<tr class=\"DataGrid_SearchRow_Title\"><td>&nbsp;$Title_Search</td></tr>
					<tr><td>$SearchHTML<div align=\"right\"><br>".CTL_InputSubmit("", "Show/Search")." from ".CTL_InputSubmit($Name="", $Value="<<", $Title="", $Size="", $Class="FormInputButton", $Style="", $OnClick="frmDataGridSearch$Entity.RecordShowFrom.value=parseInt(frmDataGridSearch$Entity.RecordShowFrom.value)-parseInt(frmDataGridSearch$Entity.RecordShowUpTo.value)")."".CTL_InputText($Name="RecordShowFrom", $DefaultValue="1", $Title="", $Size="5")."".CTL_InputSubmit($Name="", $Value=">>", $Title="", $Size="", $Class="FormInputButton", $Style="", $OnClick="frmDataGridSearch$Entity.RecordShowFrom.value=parseInt(frmDataGridSearch$Entity.RecordShowFrom.value)+parseInt(frmDataGridSearch$Entity.RecordShowUpTo.value)")." upto ".CTL_InputText($Name="RecordShowUpTo", $DefaultValue="20", $Title="", $Size="5")." record(s) ".CTL_InputSubmit($Name="", $Value="Show all", $Title="", $Size="", $Class="FormInputButton", $Style="", $OnClick="frmDataGridSearch$Entity.RecordShowUpTo.value=''")."<div></td></tr>
				</form>";
		$HTML_ControlPanel="
			        <tr class=\"DataGrid_ControlPanel_Row\">
						<td>$ControlHTML</td>
					</tr>";
		if(!$SearchPanel)$HTML_Search="";
		if(!$ControlPanel)$HTML_ControlPanel="";
		$HTML_Grid="
		    <table border=\"0\" cellspacing=\"0\" class=\"DataGrid_Table\" align=\"center\">
		        $HTML_Search
		        <form name=\"frmDataGrid$Entity\" action=\"$FormActionURL\" method=\"post\" enctype=\"multipart form/data\">
			        <tr>
						<td>
						    <table cellspacing=\"0\" class=\"DataGrid_Title_Table\">
						        <tr>
						            <td class=\"DataGrid_Title_Row_Left\">&nbsp;$EntityAlias list</td>
						            <td class=\"DataGrid_Title_Row_Right\">Total of ".count($Rows)." record(s) found&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					$HTML_ControlPanel
					<tr>
					    <td>
					        <table border=\"0\" cellspacing=\"0\" class=\"DataGrid_GridTable\">
						        <tr class=\"DataGrid_ColumnTitle_Row\">
						            <td class=\"DataGrid_ColumnTitle_Row_Serial_Cell\">Sl.</td>
								    <td class=\"DataGrid_ColumnTitle_Row_CheckBox_Cell\">
		";
		if($CheckBox)$HTML_Grid.="<input type=\"checkbox\" name=\"chk".$Entity."_SelectToggle\" class=\"FormInputCheck\" onclick=\"SetSeriesCheckBoxChecked('frmDataGrid$Entity', 'chk".$Entity."_', 0, ".count($Rows).", this.checked)\">";
		$HTML_Grid.="
									</td>";
		foreach($ColumnName as $ThisColumn){
			$HTML_Grid.="<td width=\"";
		    if($ColumnType[array_search($ThisColumn, $ColumnName)]!="email" and $ColumnType[array_search($ThisColumn, $ColumnName)]!="file"){
				$HTML_Grid.="\"><a href=\"".ApplicationURL($Script=$_REQUEST["Script"], "RecordShowFrom={$_REQUEST["RecordShowFrom"]}&RecordShowUpTo={$_REQUEST["RecordShowUpTo"]}&SortBy=$ThisColumn&SortType=$ReverseSortType&$SortLinkExtraParameter")."\" class=\"DataGrid_ColumnTitle_Link\">".$ColumnTitle[array_search($ThisColumn, $ColumnName)]."</a>";
				if($ThisColumn==$_REQUEST["SortBy"]){
					$HTML_Grid.="<img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid/datagrid_sortorder_".strtolower($_REQUEST["SortType"]).".gif\">";
				}
			}else{$HTML_Grid.="1\">";}
			$HTML_Grid.="</td>";
		}
		if($ActionLinks or count($AdditionalLinks)>0)$HTML_Grid.="<td class=\"DataGrid_ColumnTitle_Row_Action_Cell\">&nbsp;</td>";
		$HTML_Grid.="			</tr>";
		$RowCounter=0;
		foreach($Rows as $Row){
		    $RowCounter++;
		    if(fmod($RowCounter+2, 2)==0){$DataRowHightLightType="Even";}else{$DataRowHightLightType="Odd";}
		    $HTML_Grid.="		<tr class=\"DataGrid_DataRow_$DataRowHightLightType\">
								    <td>$RowCounter</td>
								    <td>
			";
			if($CheckBox)$HTML_Grid.="<input type=\"checkbox\" name=\"chk".$Entity."_$RowCounter\" value=\"".$Row[$Entity."ID"]."_".$Row[$Entity."UUID"]."\" class=\"FormInputCheck\">";
			$HTML_Grid.="
									</td>";
			foreach($ColumnName as $ThisColumn){
			    if($ColumnType[array_search($ThisColumn, $ColumnName)]!="Email"){
				    $HTML_Grid.="<td class=\"DataGrid_DataCell\" align=\"".$ColumnAlign[array_search($ThisColumn, $ColumnName)];
				}else{
				    $HTML_Grid.="<td width=\"1";
				}
			    $HTML_Grid.="\">";
				switch(trim(strtolower($ColumnType[array_search($ThisColumn, $ColumnName)]))){
	   			case "text":
					$HTML_Grid.=$Row[$ThisColumn];
				    break;
	   			case "date":
					if($Row[$ThisColumn] == NULL){
						$HTML_Grid.="";	
					}else{
						$HTML_Grid.=date("M j, y", strtotime($Row[$ThisColumn]));
					}
				    break;
	   			case "email":
					$HTML_Grid.="<a href=\"mailto:".$Row[$ThisColumn]."\" class=\"\" title=\"Send email\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid/datagrid_action_email.gif\"></a>";
				    break;
	   			case "url":
					$HTML_Grid.="<a href=\"".$Row[$ThisColumn]."\" target=\"_blank\" class=\"\" title=\"Visit web site\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid_action_web.gif\"></a>";
				    break;
				case "yes/no":
				    $HTML_Grid.="<img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid/datagrid_icon_";
				    if($Row[$ThisColumn]==1){$HTML_Grid.="yes";}else{$HTML_Grid.="no";}
					$HTML_Grid.=".gif\">";
				    break;
				case "file":
				    if(file_exists($Application["UploadPath"].$Row[$ThisColumn])&&$Row[$ThisColumn]){
						$HTML_Grid.="<a href=\"".$Application["UploadPath"].$Row[$ThisColumn]."\" class=\"\" title=\" Download ".$Row[$ThisColumn]." \"><img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid_action_download.gif\"></a>";
					}else{
						$HTML_Grid.="<img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid/datagrid_icon_notavailable.gif\">";
					}
				    break;
				case "imagelink":
				    if(file_exists($Application["UploadPath"].$Row[$ThisColumn])&&$Row[$ThisColumn]){
						$HTML_Grid.="<a href=\"".$Application["UploadPath"].$Row[$ThisColumn]."\" class=\"\" title=\" Click to view full size \" target=\"_blank\"><img src=\"".$Application["UploadPath"].$Row[$ThisColumn]."\" class=\"DataGrid_Image\"></a>";
					}else{
						$HTML_Grid.="<img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid/datagrid_icon_notavailable.gif\">";
					}
				    break;
				}
				$HTML_Grid.="</td>";
			}
			if($ActionLinks or count($AdditionalLinks)>0)$HTML_Grid.="<td class=\"DataGrid_ActionCell\">";
			$ActionParameter=$Entity."ID=".$Row[$Entity."ID"]."&".$Entity."UUID=".$Row[$Entity."UUID"];
			if($AdditionalActionParameter!="")$ActionParameter.="&$AdditionalActionParameter";
			foreach($AdditionalLinks as $AdditionalLink){$HTML_Grid.="<a href=\"".ApplicationURL($Script=$AdditionalLink["Action"] , $ActionParameter."&".$AdditionalLink["Parameter"])."\" title=\"{$AdditionalLink["Tooltip"]}\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid/datagrid_action_".$AdditionalLink["Image"].".gif\"></a>";}
			if($ActionLinks){
				$HTML_Grid.="				<!-- <a href=\"".ApplicationURL($Script=$EntityLower."detail", $ActionParameter)."\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid/datagrid_action_detail.gif\"></a> -->
											<a href=\"".ApplicationURL($Script=$EntityLower."insertupdate", $ActionParameter)."\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid/datagrid_action_edit.gif\" alt=\"Edit\"></a>
											<a href=\"".ApplicationURL($Script=$EntityLower."delete", $ActionParameter)."\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/datagrid/datagrid_action_delete.gif\" alt=\"Delete\"></a>";
			}
			if($ActionLinks or count($AdditionalLinks)>0)$HTML_Grid.="</td>";
			$HTML_Grid.="</tr>";
		}
		$HTML_Grid.="		</table>
						</td>
					</tr>
					<tr><td class=\"DataGrid_FooterRow\"></td></tr>
				</form>
			</table>";
		return $HTML_Grid;
	}
	function CTL_Thumbnail($Thumbnail, $Width=148, $Height=148, $ThumbnailStyle="standard", $BackgroundColor="White"){

		DebugFunctionTrace($FunctionName="CTL_Thumbnail", $Parameter=array("Thumbnail"=>$Thumbnail, "Width"=>$Width, "Height"=>$Height, "ThumbnailStyle"=>$ThumbnailStyle, "BackgroundColor"=>$BackgroundColor), $UseURLDebugFlag=true);


	    $HTML="
	        <!-- Thumbnail start -->
			<table cellspacing=\"0\">
				<tr>
				    <td colspan=\"99\" width=\"1\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/thumbnail_".$ThumbnailStyle."_border_top.gif\"></td>
				</tr>
				<tr>
				    <td width=\"1\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/thumbnail_".$ThumbnailStyle."_border_left.gif\"></td>
				    <td style=\"background-color: $BackgroundColor;\"><a href=\"./image/".$Thumbnail.".gif\" target=\"_blank\"><img src=\"./image/".$Thumbnail."_thumbnail.gif\" width=\"$Width\" height=\"$Height\"></a></td>
				    <td width=\"1\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/thumbnail_".$ThumbnailStyle."_border_right.gif\"></td>
				</tr>
				<tr>
				    <td colspan=\"99\" width=\"1\"><img src=\"./theme/".$_REQUEST["Theme"]."/image/thumbnail_".$ThumbnailStyle."_border_bottom.gif\"></td>
				</tr>
			</table>
	        <!-- Thumbnail end -->
		";
		return $HTML;
	}
	function CTL_ThumbnailGrid($Images, $Columns=4, $Spacing=15){

		DebugFunctionTrace($FunctionName="CTL_ThumbnailGrid", $Parameter=array("Images"=>$Images, "Columns"=>$Columns, "Spacing"=>$Spacing), $UseURLDebugFlag=true);

		$Rows=ceil(count($Images)/$Columns);
	    $ThumbnailGrid="
			<!-- Start of Thumbnail grid -->
			<table cellspacing=\"$Spacing\">
		";
		for($RowCounter=1; $RowCounter<=$Rows; $RowCounter++){
		    $ThumbnailGrid.="<tr>";
		    for($ColumnCounter=1; $ColumnCounter<=$Columns; $ColumnCounter++){
		        $CurrentImage=(($RowCounter*$Columns)-$Columns)+$ColumnCounter;
		        if($CurrentImage<=count($Images)){
		            $ThumbnailGrid.="<td>".CTL_Thumbnail($Images[$CurrentImage-1])."</td>";
				}
			}
		    $ThumbnailGrid.="</tr>";
		}
	    $ThumbnailGrid.="
			</table>
			<!-- End of Thumbnail grid -->
		";
		return $ThumbnailGrid;
	}
?>