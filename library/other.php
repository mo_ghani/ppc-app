<?

	function EPLS_Record($SSNName, $SSNTIN, $Status="current", $Start="ssn", $SSN="true"){
		$Response=strip_tags(file_get_contents("http://www.epls.gov/epls/search.do?ssn_name=$SSNName&ssn_tin=$SSNTIN&status=$Status&start=$Start&ssn=$SSN"));
		$Found=true;
		if(!strpos($Response, "Your search returned no results"))$Found=false;
		return array("HTML"=>$Response, "Found"=>$Found);
	}

	//Build the application URL specially formatted to fit the FuseBox manner
	function ApplicationURL($Script="", $OtherParameter="", $Section="", $PathOnly=false){

		DebugFunctionTrace($FunctionName="ApplicationURL", $Parameter=array("Script"=>$Script, "OtherParameter"=>$OtherParameter, "Section"=>$Section, "PathOnly"=>$PathOnly), $UseURLDebugFlag=true);

	    $URL="http";
	    if(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)=="HTTPS"){$URL.="s";}
	    $URL.="://";
		$URL.=$_SERVER["HTTP_HOST"];
		if(!$PathOnly){$URL.=$_SERVER["PHP_SELF"];}else{$URL.=ScriptPath();}
		$URL.="?NoParameter";
		if(isset($_REQUEST["Theme"]))$URL.="&Theme={$_REQUEST["Theme"]}";
		if($Script!="")$URL.="&Script=$Script";
		if($OtherParameter!=""){$URL.="&$OtherParameter";}
		if(isset($_REQUEST["Debug"])){$URL.="&Debug";}
		if(isset($_REQUEST["MainContentOnly"])){$URL.="&MainContentOnly";}
		if(isset($_REQUEST["NoHeader"])){$URL.="&NoHeader";}
		if(isset($_REQUEST["NoFooter"])){$URL.="&NoFooter";}
		if($Section!=""){$URL.="#$Section";}
		
		return $URL;
	}
	
	//Generate a system wide unique identifier
	function GUID($CurlyBrace=false){

		//DebugFunctionTrace($FunctionName="GUID", $Parameter=array("CurlyBrace"=>$CurlyBrace), $UseURLDebugFlag=true);

		if (function_exists('com_create_guid')){
			return com_create_guid();
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$CharacterID = strtoupper(md5(uniqid(rand(), true)));
			$Hyphen = chr(45);// "-"
			$GUID = substr($CharacterID, 0, 8).$Hyphen
			.substr($CharacterID, 8, 4).$Hyphen
			.substr($CharacterID,12, 4).$Hyphen
			.substr($CharacterID,16, 4).$Hyphen
			.substr($CharacterID,20,12);// "}"
			if($CurlyBrace)$GUID="{".$GUID."}";
			return $GUID;
		}
	}
?>
