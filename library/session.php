<?
	
	function SessionSet(){
	    global $Application;

		session_start();
		if(!isset($_SESSION["UserTypeID"]))		$_SESSION["UserTypeID"]=	$Application["UserTypeIDGuest"];
		if(!isset($_SESSION["UserTypeName"]))	$_SESSION["UserTypeName"]=	$Application["UserTypeNameGuest"];
		if(!isset($_SESSION["UserID"]))			$_SESSION["UserID"]=		$Application["UserIDGuest"];
		if(!isset($_SESSION["UserUUID"]))		$_SESSION["UserUUID"]=		$Application["UserUUIDGuest"];
		if(!isset($_SESSION["UserName"]))		$_SESSION["UserName"]=		$Application["UserNameGuest"];
		if(!isset($_SESSION["UserPassword"]))	$_SESSION["UserPassword"]=		"";
		if(!isset($_SESSION["UserEmail"]))		$_SESSION["UserEmail"]=			"";
		if(!isset($_SESSION["Name"]))		$_SESSION["Name"]=			"";
		//Date & time stamp of the last operation, works in association with $_SESSION["SessionTimeout"] to implement the automatic session expiration
		if(!isset($_SESSION["DateTimeLastUserAction"])){$_SESSION["DateTimeLastUserAction"]=  date("m/d/y H:i:s");}
		//Force user log off if the current session is timed out
	    SessionTimeout();
	}
	
	function SessionSetTimeStamp(){
		$_SESSION["DateTimeLastUserAction"]=date("m/d/y H:i:s");
	}
	
	function SessionTimeout(){
	    global $Application;

		$DateTimeActivityDifference=FN_DateTimeDifference(date("m/d/y H:i:s"), $_SESSION["DateTimeLastUserAction"]);
		if($DateTimeActivityDifference["Minutes"]>$Application["SessionTimeout"])SessionUnsetUser();
	}
	
	function SessionSetUser($UserRow){
	    //$User (array of colum values)

	    $_SESSION["UserTypeID"]=	$UserRow["UserTypeID"];
	    $_SESSION["UserTypeName"]=	$UserRow["UserTypeName"];
	    $_SESSION["UserID"]=		$UserRow["UserID"];
	    $_SESSION["UserUUID"]=		$UserRow["UserUUID"];
	    $_SESSION["UserName"]=		$UserRow["UserName"];
	    $_SESSION["UserEmail"]=		$UserRow["UserEmail"];
	    $_SESSION["UserPassword"]=	$UserRow["UserPassword"];
	    $_SESSION["Name"]=		$UserRow["Name"];
	}
	
	function SessionUnsetUser(){
	    global $Application;
	    
		$UserQuery="Update tbluser SET IsLogin = 0 WHERE UserID=".$_SESSION["UserID"]."";
		$UserUpdate=mysql_query($UserQuery);
		
		$_SESSION["UserTypeID"]=	$Application["UserTypeIDGuest"];
		$_SESSION["UserTypeName"]=	$Application["UserTypeNameGuest"];
		$_SESSION["UserID"]=		$Application["UserIDGuest"];
		$_SESSION["UserUUID"]=		$Application["UserUUIDGuest"];
		$_SESSION["UserName"]=		$Application["UserNameGuest"];
		$_SESSION["UserPassword"]=	$Application["UserPasswordGuest"];
		$_SESSION["UserEmail"]=		$Application["UserEmailGuest"];
	}
?>
