<?
	function CheckRequiredFormVariables($Variable){
		DebugFunctionTrace($FunctionName="CheckRequiredFormVariables", $Parameter=array("Variable"=>$Variable), $UseURLDebugFlag=true);

	    global $ErrorUserInput;
		foreach($Variable as $ThisVariable){
		    if(trim($_POST[$ThisVariable["Name"]])==""){
			    //print "'{$ThisVariable["Name"]}' = '".trim($_POST[$ThisVariable["Name"]])."' found NULL<hr>";
		        $ErrorUserInput["_Error"]=true;
		        $ErrorUserInput["_Message"]=$ThisVariable["Message"];
		        $ErrorUserInput[$ThisVariable["Name"]]=true;
			}
		}
	}

	function SetFormVariable($VariableName, $DefaultValue="", $SetErrorFlag=true, $UseRequestVariable=true, $Debug=false){

		DebugFunctionTrace($FunctionName="SetFormVariable", $Parameter=array("VariableName"=>$VariableName, "DefaultValue"=>$DefaultValue, "SetErrorFlag"=>$SetErrorFlag, "UseRequestVariable"=>$UseRequestVariable, "Debug"=>$Debug), $UseURLDebugFlag=true);

	    if(isset($_REQUEST[$VariableName]))$Debug_1="\$_REQUEST[\"$VariableName\"] = '{$_REQUEST[$VariableName]}' is set, skipping \$DefaultValue";
	    if(!isset($_REQUEST[$VariableName]))$Debug_1="\$_REQUEST[\"$VariableName\"] is NOT set, setting \$DefaultValue";

	    global $ErrorUserInput;
	    if($SetErrorFlag){
			if(!isset($ErrorUserInput["_Error"]))$ErrorUserInput["_Error"]=false;
		    if(!isset($ErrorUserInput[$VariableName]))$ErrorUserInput[$VariableName]=false;
		}
	    if($UseRequestVariable)if(!isset($_REQUEST[$VariableName]))$_REQUEST[$VariableName]=$DefaultValue;
	    if(!isset($_POST[$VariableName])){
			$_POST[$VariableName]=$DefaultValue;
	        if($UseRequestVariable)$_POST[$VariableName]=$_REQUEST[$VariableName];
		}
		if($Debug)print "
		    SetFormVariable($VariableName='$VariableName', \$DefaultValue='$DefaultValue', \$SetErrorFlag=$SetErrorFlag, \$UseRequestVariable=$UseRequestVariable, \$Debug=$Debug){<br>
		        $Debug_1<br>
		        \$_REQUEST[\"$VariableName\"] = '{$_REQUEST["$VariableName"]}';<br>
		        \$_POST[\"$VariableName\"] = '{$_POST["$VariableName"]}';<br>
			}
			<hr>
		";
	}
	
	function FormTitleRow($FormTitle){
		DebugFunctionTrace($FunctionName="FormTitleRow", $Parameter=array("FormTitle"=>$FormTitle), $UseURLDebugFlag=true);

	    return "<tr class=\"FormRowTitle\"><td>$FormTitle</td></tr>";
	}
	
	function FormErrorRow($EntityName){
		DebugFunctionTrace($FunctionName="FormErrorRow", $Parameter=array("EntityName"=>$EntityName), $UseURLDebugFlag=true);
	   
		global $ErrorUserInput;
	    $HTML="";
	    if($ErrorUserInput["_Error"])$HTML="<tr class=\"FormRowErrorMessage\"><td>{$ErrorUserInput["_Message"]}</td></tr>";
	    return $HTML;
	}
	
	function FormInputSectionRow($Caption=""){
		
		DebugFunctionTrace($FunctionName="FormInputSectionRow", $Parameter=array("Caption"=>$Caption), $UseURLDebugFlag=true);
    return "<tr class=\"FormRowInputSection\"><td colspan=\"3\">$Caption</td></tr>";
	}
	
	function FormInputRow($VariableName, $Caption, $ControlHTML, $Required=false){

	   DebugFunctionTrace($FunctionName="FormInputRow", $Parameter=array("VariableName"=>$VariableName, "Caption"=>$Caption, "ControlHTML"=>$ControlHTML, "Required"=>$Required), $UseURLDebugFlag=true);

	    global $ErrorUserInput;
	    $RequiredSymbol="&nbsp;";
	    if($Required)$RequiredSymbol="*";
	    
	    $HTML="<tr class=\"FormRowField\"><td";
		if($ErrorUserInput[$VariableName])$HTML.=" class=\"FormFieldCaptionError\"";
		$HTML.=">$Caption</td><td>$ControlHTML</td><td class=\"FormFieldRequired\">$RequiredSymbol</td></tr>";
		
		return $HTML;
	}
	
	function FormButtonRow($ButtonCaption){
		DebugFunctionTrace($FunctionName="FormButtonRow", $Parameter=array("ButtonCaption"=>$ButtonCaption), $UseURLDebugFlag=true);

	    return "<tr class=\"FormRowButton\"><td>".CTL_InputSubmit("", $ButtonCaption)."</td></tr>";
	}

	function FormInsertUpdate($EntityName, $FormTitle, $Input, $ButtonCaption, $ActionURL){
	  
		DebugFunctionTrace($FunctionName="FormInsertUpdate", $Parameter=array("EntityName"=>$EntityName,"FormTitle"=>$FormTitle,"Input"=>$Input,"ButtonCaption"=>$ButtonCaption,"ActionURL"=>$ActionURL), $UseURLDebugFlag=true);

		global $ErrorUserInput;

	    $JavaScriptInputvalidator="";

		if(!isset($ErrorUserInput["_Error"]))$ErrorUserInput["_Error"]=false;
	    $HTML="
	        <div align=\"center\">
				<table cellspacing=\"0\" width=\"100%\">
					<form name=\"frm".$EntityName."InsertUpdate\" action=\"$ActionURL\" method=\"post\" enctype=\"multipart/form-data\" onSubmit=\"return Validate".$EntityName."Input()\">
					    ".FormTitleRow($FormTitle)."
					    ".FormErrorRow($EntityName)."
						<tr>
						    <td>
						        <table class=\"FormTextNormal\">
		";
		foreach($Input as $ThisInput){
		    if($ThisInput["VariableName"]=="SectionTitleRow"){
		        $HTML.=FormInputSectionRow($Caption=$ThisInput["Caption"]);
			}else{
			    SetFormVariable($ThisInput["VariableName"], $ThisInput["DefaultValue"], $SetErrorFlag=true, $UseRequestVariable=true);

			    if($ThisInput["Required"])$JavaScriptInputvalidator.="
					if(frm".$EntityName."InsertUpdate.{$ThisInput["VariableName"]}.value==''){InputValid=false; WarningMessage+='    * ".str_replace("'", "\'", $ThisInput["Caption"]).".\\n';}
				";
			    $HTML.=FormInputRow($ThisInput["VariableName"], $Caption=$ThisInput["Caption"], $ControlHTML=$ThisInput["ControlHTML"], $Required=$ThisInput["Required"]);
			}
		}
	    $HTML.="
								</table>
							</td>
						</tr>
					    ".FormButtonRow($ButtonCaption)."
					</form>
				</table>
			</div>

			<script language=\"JavaScript\">
			<!--
				function Validate".$EntityName."Input(){
					var InputValid=true; WarningMessage='Please provide with the following parameters;\\n\\n';
$JavaScriptInputvalidator
					if(!InputValid)alert(WarningMessage);
					return InputValid;
				}
			\\-->
			</script>
		";
		
		return $HTML;
	}
?>