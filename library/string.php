<?

	//Generate a random string. Generally, as a password
	function RandomPassword(){
		$chars = "ABCDEFGHIJKMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz023456789~!@#$%^&*";
		srand((double)microtime()*1000000);
		$i = 0;
		$pass = '' ;
		while($i <= 7){
			$num = rand() % 70;
			$tmp = substr($chars, $num, 1);
			$pass = $pass.$tmp;
			$i++;
		}
		return $pass;
	}

	//Translate all the blank spaces to HTML blank spaces '&nbsp;'
	function UnWrap($TextToUnWrap){
		return str_replace(" ", "&nbsp;", $TextToUnWrap);
	}

	function CRLFToBR($TextToTranslate){
		return str_replace(chr(13).chr(10), "<br>", $TextToTranslate);
	}

	//An extended string search function
    function SearchStringEx($LookFor, $LookIn, $StringComparisonMode="SCTM_PARTIAL"){
        $MatchPosition=-1;

        if($StringComparisonMode=='SCTM_EXACT'){
            if($LookIn==$LookFor){$MatchPosition=0;}
        }elseif($StringComparisonMode=='SCTM_PARTIAL'){
            if(strpos($LookIn, $LookFor)){$MatchPosition=strpos($LookIn, $LookFor);}
        }elseif($StringComparisonMode=='SCTM_LEFT'){
            if(substr($LookIn, 0, strlen($LookFor))){$MatchPosition=0;}
        }elseif($StringComparisonMode=='SCTM_RIGHT'){
            if(substr($LookIn, strlen($LookIn)-strlen($LookFor), strlen($LookFor))){$MatchPosition=strlen($LookIn)-strlen($LookFor);}
        }

        //if($MatchPosition>-1){print "Match found! Looking for '$LookFor' in '$LookIn' as '$StringComparisonMode'<br>";}

        return $MatchPosition;
    }
?>
