<?

	$SQL_SelectStatement=array(

	    "ApplicationSetting"=>"

	        SELECT APS.* FROM tblapplicationsetting AS APS

	        LEFT JOIN tbluser AS UI ON UI.UserID = APS.UserIDInserted

	        LEFT JOIN tbluser AS UU ON UU.UserID = APS.UserIDUpdated

	        LEFT JOIN tbluser AS UL ON UL.UserID = APS.UserIDLocked

		",



	    "User"=>"

	        SELECT		U.*,

						UT.UserTypeName,

						C.CountryName

			FROM		tbluser AS U

	        LEFT JOIN	tblcountry AS C ON C.CountryID = U.CountryID

	        LEFT JOIN	tblusertype AS UT ON UT.UserTypeID = U.UserTypeID

	        LEFT JOIN	tbluser AS UI ON UI.UserID = U.UserIDInserted

	        LEFT JOIN	tbluser AS UU ON UU.UserID = U.UserIDUpdated

	        LEFT JOIN	tbluser AS UL ON UL.UserID = U.UserIDLocked

		",



	    "UserType"=>"

	        SELECT UT.* FROM tblusertype AS UT

	        LEFT JOIN tbluser AS UI ON UI.UserID = UT.UserIDInserted

	        LEFT JOIN tbluser AS UU ON UU.UserID = UT.UserIDUpdated

	        LEFT JOIN tbluser AS UL ON UL.UserID = UT.UserIDLocked

		",



	    "Country"=>"

	        SELECT CN.* FROM tblcountry AS CN

	        LEFT JOIN tbluser AS UI ON UI.UserID = CN.UserIDInserted

	        LEFT JOIN tbluser AS UU ON UU.UserID = CN.UserIDUpdated

	        LEFT JOIN tbluser AS UL ON UL.UserID = CN.UserIDLocked

		",

		

		"Bank"=>"

	        SELECT B.*,CT.CountryName FROM tblbank AS B

	        LEFT JOIN tblcountry AS CT ON CT.CountryID = B.BankCountryID

		",

		

		"Tours"=>"

	        SELECT T.* FROM tbltours AS T

		",

		

		"PinCode"=>"

	        SELECT PC.*,CU.UserName as Creator,UU.UserName as User,RK.RankName FROM tblpincode AS PC

			LEFT JOIN tbluser AS CU ON CU.UserID = PC.CreateUserID

			LEFT JOIN tbluser AS UU ON UU.UserID = PC.UsedUserID

			LEFT JOIN tblrank AS RK ON RK.RankID = PC.RankID

		",



	    "Payment"=>"

	        SELECT P.*,U.UserName,U.Name

			FROM tbluser as U

	        LEFT JOIN tblpayment AS P ON P.UserID = U.UserID

		",
		
		"Rank"=>"
	        SELECT R.*
			FROM tblrank as R
		",


		"RankB"=>"
	        SELECT R.*
			FROM tblrank as R where RankName not in ('Bronze')
		",

		"Language"=>"

	        SELECT L.* FROM tbllanguage AS L

	        LEFT JOIN tbluser AS UI ON UI.UserID = L.UserIDInserted

	        LEFT JOIN tbluser AS UU ON UU.UserID = L.UserIDUpdated

	        LEFT JOIN tbluser AS UL ON UL.UserID = L.UserIDLocked

		",





		"StaticContent"=>"

	        SELECT SC.*, L.LanguageCode

			FROM tblstaticcontent AS SC

   	        LEFT JOIN tbllanguage AS L ON L.LanguageID = SC.LanguageID

	        LEFT JOIN tbluser AS UI ON UI.UserID = SC.UserIDInserted

	        LEFT JOIN tbluser AS UU ON UU.UserID = SC.UserIDUpdated

	        LEFT JOIN tbluser AS UL ON UL.UserID = SC.UserIDLocked

		",



		"AdvertPanel"=>"

	        SELECT AP.*,

	        CONCAT(AP.AdvertPanelName, ' (', AP.AdvertWidth, 'x', AP.AdvertHeight,')') AS AdvertPlacementName

			FROM tbladvertpanel AS AP

	        LEFT JOIN tbluser AS UI ON UI.UserID = AP.UserIDInserted

	        LEFT JOIN tbluser AS UU ON UU.UserID = AP.UserIDUpdated

	        LEFT JOIN tbluser AS UL ON UL.UserID = AP.UserIDLocked

		",



		"Advert"=>"

	        SELECT A.*,

	        AP.AdvertPanelName

			FROM tbladvert AS A

	        LEFT JOIN tbladvertpanel AS AP ON AP.AdvertPanelID = A.AdvertPanelID

	        LEFT JOIN tbluser AS UI ON UI.UserID = A.UserIDInserted

	        LEFT JOIN tbluser AS UU ON UU.UserID = A.UserIDUpdated

	        LEFT JOIN tbluser AS UL ON UL.UserID = A.UserIDLocked

		",

		

		"AdvertRec"=>"

	        SELECT AR.*,

	        UI.UserName

			FROM tbladvertrec AS AR

	        LEFT JOIN tbladvert AS A ON A.AdvertID = AR.AdvertID

	        LEFT JOIN tbluser AS UI ON UI.UserID = AR.UserID

		",



	);



//	        CONCAT(AP.AdvertPanelName, ' (', AP.AdvertWidth, 'x', AP.AdvertHeight,')') AS AdvertPlacementName





	function SQL_CleanUp($Where="", $CleanUp=true, $Debug=false){

	    //Clean up orphan records and uploaded files

	}

?>