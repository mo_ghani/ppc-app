<?

	//Set the publicly accessible pages

	$AccessiblePage=array(

		"home",

		"login", "loginaction", "loginretrieve","renewmembership",

		"usersignup", "usersignupaction", "usersignupconfirm",

		"userpasswordrecover", "userpasswordrecoveraction",

		"paypalipn", "paymentlander", "whoweare", "profile", "mission", "ourteam", "contactus", "contactconfirm", "business",

		"motorcycle", "tv", "fridge", "car", "solar", "cosmetics", "software", "mobile", "businessplan",

		"about","policy","products", "popup"

	);

	//Set the pages accessible for the ADMINISTRATOR users only

	if($_SESSION["UserTypeID"]==$Application["UserTypeIDAdministrator"])$AccessiblePage=array_merge(

		$AccessiblePage,

		array(

			"applicationsettingmanage", "applicationsettinginsertupdate", "applicationsettinginsertupdateaction", "applicationsettingdelete", "applicationsettingdeleteaction",

			"applicationsettingtypemanage", "applicationsettingtypeinsertupdate", "applicationsettingtypeinsertupdateaction", "applicationsettingtypedelete", "applicationsettingtypedeleteaction",

			"usermanage", "userinsertupdate", "userinsertupdateaction", "userdelete", "userdeleteaction",

			"usertypemanage", "usertypeinsertupdate", "usertypeinsertupdateaction", "usertypedelete", "usertypedeleteaction",

			"surveymanage", "surveyinsertupdate", "surveyinsertupdateaction", "surveydelete", "surveydeleteaction",

			"surveyquestioncategorymanage", "surveyquestioncategoryinsertupdate", "surveyquestioncategoryinsertupdateaction", "surveyquestioncategorydelete", "surveyquestioncategorydeleteaction",

			"surveyquestiongroupmanage", "surveyquestiongroupinsertupdate", "surveyquestiongroupinsertupdateaction", "surveyquestiongroupdelete", "surveyquestiongroupdeleteaction",

			"surveyquestiongroupassignsurveyquestion",

			"surveyquestionmanage", "surveyquestioninsertupdate", "surveyquestioninsertupdateaction", "surveyquestiondelete", "surveyquestiondeleteaction",

			"surveyquestionanswermanage", "surveyquestionanswerinsertupdate", "surveyquestionanswerinsertupdateaction", "surveyquestionanswerdelete", "surveyquestionanswerdeleteaction",

			"pointrangeevaluationsurveymanage", "pointrangeevaluationsurveyinsertupdate", "pointrangeevaluationsurveyinsertupdateaction", "pointrangeevaluationsurveydelete", "pointrangeevaluationsurveydeleteaction",

			"pointrangeevaluationsurveyquestionmanage", "pointrangeevaluationsurveyquestioninsertupdate", "pointrangeevaluationsurveyquestioninsertupdateaction", "pointrangeevaluationsurveyquestiondelete", "pointrangeevaluationsurveyquestiondeleteaction",

			"pointrangeevaluationsurveyquestioncategorymanage", "pointrangeevaluationsurveyquestioncategoryinsertupdate", "pointrangeevaluationsurveyquestioncategoryinsertupdateaction", "pointrangeevaluationsurveyquestioncategorydelete", "pointrangeevaluationsurveyquestioncategorydeleteaction",

			"pointrangeevaluationsurveyquestiongroupmanage", "pointrangeevaluationsurveyquestiongroupinsertupdate", "pointrangeevaluationsurveyquestiongroupinsertupdateaction", "pointrangeevaluationsurveyquestiongroupdelete", "pointrangeevaluationsurveyquestiongroupdeleteaction",

			"surveyquestionlinkmanage", "surveyquestionlinkinsertupdate", "surveyquestionlinkinsertupdateaction",

			"surveyquestionlinkselect", "surveyquestionlinkdelete", "surveyquestionlinkdeleteaction",

			"reportsurveydelete", "reportsurveydeleteaction",

			"questionexport", "questionexportaction",

			"paymentmanage", "paymentdelete", "paymentdeleteaction","paymentupdate","paymentupdateaction",

			"couponmanage", "couponinsertupdate", "couponinsertupdateaction", "coupondelete", "coupondeleteaction",

			"teammanage", "teaminsertupdate", "teaminsertupdateaction", "teamdelete", "teamdeleteaction",

			"teamsurveyselect", "teamsurveyreport","adminhome",

			"advertpanelmanage", "advertpanelinsertupdate", "advertpanelinsertupdateaction", "advertpaneldelete", "advertpaneldeleteaction",

			"advertmanage", "advertinsertupdate", "advertinsertupdateaction", "advertdelete", "advertdeleteaction",

			"bankmanage", "bankinsertupdate", "bankinsertupdateaction", "bankdelete", "bankdeleteaction",

			"staticcontentedit", "staticcontenteditaction", "imagestorebrowser",

			"memberprofile","memberprofileupdate",

			"pincodemanage","pincodeinsert","pincodedelete","pincodedeleteaction",

			"sitesetting","withdrawpayment","withdrawpaymentaction","withdrawEpayment","withdrawEpaymentaction",

			"investsetting","balancezero","balancezeroaction","message",

			"toursmanage", "toursinsertupdate", "toursinsertupdateaction", "toursdelete", "toursdeleteaction",

			"tourbuy","tourbuyaction","transferAdminReport","investmentactive"

		)

	);

	//Set the pages accessible for anyone other than the GUEST

	if($_SESSION["UserTypeID"]!=$Application["UserTypeIDGuest"])$AccessiblePage=array_merge(

		$AccessiblePage,

		array(

			"logout", "survey", "surveyagreement", "surveytake", "surveytakeaction", "reportsurvey", "scoreboard",

			"userprofile", "userprofileupdate","investment","investmentaction",

			"surveypay", "checksurveypermission", "surveypayform", "surveypayformaction",

			"revokeaccount", "revokeaccountaction","pincode","pincodeaction","changepass","changetpass","transfer",

			"withdraw","withdrawE","binarytree",

			"downline","memberhome","invoice","investreport","tourreport",

			"transreport","withdrawreport","withdrawEreport","matchingreport","transferreport","sponsorreport","advertisement","advertisementlist","advertisementaction"

		)

	);



	if(!isset($_REQUEST["Script"])||!in_array($_REQUEST["Script"], $AccessiblePage))$_REQUEST["Script"]=$AccessiblePage[0];

?>